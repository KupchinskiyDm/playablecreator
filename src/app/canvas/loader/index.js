const PIXIResLoader = (function (){

    let resources = null
    let loader

    this.setLoader = function (ldr) {
        loader = ldr
    }

    this.load = function (list, callback) { 
        loader.reset()
        loader.resources = {}

        for (var textureUrl in PIXI.utils.BaseTextureCache) {
            delete PIXI.utils.BaseTextureCache[textureUrl];
        }
        for (var textureUrl in PIXI.utils.TextureCache) {
            delete PIXI.utils.TextureCache[textureUrl];
        }

        list.forEach(element => {
            try {
                loader.add(element, element)
            } catch (error) {
                console.warn(error.message)
            }
            
        });

        loader.add('image.png', 'res/image.png')
        loader.add('grid.png', 'res/grid.png')
        loader.add('mv_quad.png', 'res/mv_quad.png')
        loader.add('y_arrow.png', 'res/y_arrow.png')
        loader.add('x_arrow.png', 'res/x_arrow.png')
        loader.add('sy_arrow.png', 'res/sy_arrow.png')
        loader.add('sx_arrow.png', 'res/sx_arrow.png')
        loader.add('mv_quad_h.png', 'res/mv_quad_h.png')
        loader.add('y_arrow_h.png', 'res/y_arrow_h.png')
        loader.add('x_arrow_h.png', 'res/x_arrow_h.png')
        loader.add('sy_arrow_h.png', 'res/sy_arrow_h.png')
        loader.add('sx_arrow_h.png', 'res/sx_arrow_h.png')
        loader.add('inst_scale_h', 'res/inst_scale_h.png')
        loader.add('inst_scale', 'res/inst_scale.png')
        loader.add('insr_rot_h', 'res/insr_rot_h.png')
        loader.add('insr_rot', 'res/insr_rot.png')
        loader.add('inst_move_h', 'res/inst_move_h.png')
        loader.add('inst_move', 'res/inst_move.png')
        loader.add('rotate_h', 'res/rotate_h.png')
        loader.add('rotate', 'res/rotate.png')
        loader.add('particle.png', 'res/particle.png')

        loader.load ((loader, res) => {
            resources = res
            Creator.eventsManager.dispatchEvent(Creator.events.PIXI_LOADER_READY, list, self)
            callback && callback.apply()
        })
    }

    this.get = function (tag){
        if (resources){
            if (resources.hasOwnProperty(tag)){
                return resources[tag]
            } else {
                console.error(`Cannot get [${tag}] from resources`) 
            }
    
        } else {
            console.error('Resources wasn`t loaded! Load resources before.') 
        }
        return null
    }
    return this
})()

module.exports.PIXIResLoader = PIXIResLoader