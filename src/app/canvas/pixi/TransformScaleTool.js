const TransformScaleTool = function(layer, handler) {

    const self = this
    const content = new PIXI.Container()
    const mv_quad = content.addChild(new PIXI.Sprite(Creator.pixiLoader.get('mv_quad.png').texture))
    const x_arrow = content.addChild(new PIXI.Sprite(Creator.pixiLoader.get('sx_arrow.png').texture))
    const y_arrow = content.addChild(new PIXI.Sprite(Creator.pixiLoader.get('sy_arrow.png').texture))
    let target = null

    layer.interactive = true

    mv_quad.interactive = true
    mv_quad.anchor.set(0, 1)
    
    x_arrow.interactive = true
    x_arrow.anchor.set(0, 0.58)
    
    y_arrow.interactive = true
    y_arrow.anchor.set(0.5, 0.97)

    let paused = false

    let move_quad = false
    mv_quad.on('mousemove', (event) => { 
        if (target && move_quad) {
            let sx = 0
            if (Math.abs(event.data.originalEvent.movementX) >= Math.abs(event.data.originalEvent.movementY)){
                sx = event.data.originalEvent.movementX
            } else {
                sx = event.data.originalEvent.movementY
            }
            sx = sx * 0.6
            moveTarget(sx, -sx)
        }
    })
    mv_quad.on("mousedown", (event) => {
        move_quad = true
        mv_quad.texture = Creator.pixiLoader.get('mv_quad_h.png').texture
    })

    let move_x = false
    x_arrow.on('mousemove', (event) => { 
        if (target && move_x) {
            moveTarget(event.data.originalEvent.movementX, 0)
        }
    })
    x_arrow.on("mousedown", (event) => {
        move_x = true
        x_arrow.texture = Creator.pixiLoader.get('sx_arrow_h.png').texture
    })


    let move_y = false
    y_arrow.on('mousemove', (event) => { 
        if (target && move_y) {
            moveTarget(0, event.data.originalEvent.movementY)
        }
    })
    y_arrow.on("mousedown", (event) => {
        move_y = true
        y_arrow.texture = Creator.pixiLoader.get('sy_arrow_h.png').texture
    })

    const canlcelMove = function() {
        move_y = false
        move_x = false
        move_quad = false
        mv_quad.texture = Creator.pixiLoader.get('mv_quad.png').texture
        x_arrow.texture = Creator.pixiLoader.get('sx_arrow.png').texture
        y_arrow.texture = Creator.pixiLoader.get('sy_arrow.png').texture
    }

    if (handler.addEventListener) {
        handler.addEventListener("mouseup", (event) => {
            canlcelMove()
        })
        handler.addEventListener("mouseleave", (event) => {
            canlcelMove()
        })
    }

    const moveTarget = function(x, y){
        if (paused) {
            return
        }
        let scale = {
            x : target.scale.x + x * 0.01,
            y : target.scale.y - y * 0.01
        }
        target.wrapper.setProperty('scale', scale)
    }

    this.setTarget = function (transform) {
        target = transform
        if(target) {
            content.rotation = target.rotation
            this.followTarget()
        } else {
            canlcelMove()
        }
    }

    this.followTarget = function (){
        if (target){
            content.position = target.getGlobalPosition()
            content.rotation = target.rotation
        }
    }

    this.getContent = function() {
        return content
    }

    this.show = function() {
        if (target){
            content.visible = true
        }
    }

    this.hide = function() {
        content.visible = false
        canlcelMove()
    }

    this.pauseMoving = function(value){
        paused = value
    }

    content.visible = false
    layer.addChild(content)

}

module.exports.TransformScaleTool = TransformScaleTool