const TransformRotateTool = function (layer, handler) {
    const self = this
    const content = layer.addChild(new PIXI.Sprite(Creator.pixiLoader.get('rotate').texture))

    let paused = false
    let target = null
    let rotate = false
    
    layer.interactive = true
    content.interactive = true
    content.anchor.set(0.5)

    content.on('mousemove', (event) => { 
        if (target && rotate) {
            rotateTarget(event.data.originalEvent.movementY)
        }
    })
    content.on("mousedown", (event) => {
        rotate = true
        content.texture = Creator.pixiLoader.get('rotate_h').texture
    })

    const canlcelRotate = function() {
        rotate = false
        content.texture = Creator.pixiLoader.get('rotate').texture
    }

    if (handler.addEventListener) {
        handler.addEventListener("mouseup", (event) => {
            canlcelRotate()
        })
        handler.addEventListener("mouseleave", (event) => {
            canlcelRotate()
        })
    }

    const rotateTarget = function(y){
        //TODO add angle clamp
        if (paused) {
            return
        }
        target.wrapper.setProperty('rotation', target.rotation + y * 0.01)
        content.rotation = target.rotation
    }

    this.setTarget = function (transform) {
        target = transform
        if(target) {
            content.rotation = target.rotation
            this.followTarget()
        } else {
            canlcelRotate()
        }
    }

    this.followTarget = function (){
        if (target){
            content.position = target.getGlobalPosition()
            content.rotation = target.rotation
        }
    }

    this.getContent = function() {
        return content
    }

    this.show = function() {
        if (target){
            content.visible = true
        }
    }

    this.hide = function() {
        content.visible = false
        canlcelRotate()
    }

    this.pauseMoving = function(value){
        paused = value
    }

    content.visible = false

}

module.exports.TransformRotateTool = TransformRotateTool