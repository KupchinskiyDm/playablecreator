const Instrumentation = function (layer, target) {
    const {TransformMoveTool} = require(path.resolve(__dirname, './TransformMoveTool'));
    const {TransformRotateTool} = require(path.resolve(__dirname, './TransformRotateTool'));
    const {TransformScaleTool} = require(path.resolve(__dirname, './TransformScaleTool'));

    const self = this
    const content = new PIXI.Container()
    const instButtons = []
    const instruments = []
    const instrumenclass = [TransformMoveTool,TransformRotateTool,TransformScaleTool]
    const textures = ['inst_move', 'insr_rot', 'inst_scale']

    let selected = 0
    let manualSelected = 0
    let ctrlMode = false
    let altMode = false

    const instrumentSelected = function(index){
        selected = index
        instButtons.forEach((btn, texture) => {
            btn.texture = Creator.pixiLoader.get(textures[texture]).texture
        })
        instButtons[index].texture = Creator.pixiLoader.get(`${textures[index]}_h`).texture
        instruments.forEach(instrument => {
            instrument.hide()
        })
        instruments[selected].followTarget()
        instruments[selected].show()
    }

    textures.forEach((texture, index) => {
        const tmp = content.addChild(new PIXI.Sprite(Creator.pixiLoader.get(texture).texture))
        tmp.anchor.set(0.5)
        tmp.interactive = true
        tmp.scale = {x: 0.5, y : 0.5}
        tmp.position = {
            x : 45 * index,
            y : 0
        }
        tmp.on("mousedown", (event) => {
            manualSelected = index
            instrumentSelected(index)
        })
        instButtons.push(tmp)
        instruments.push(new instrumenclass[index](layer, target.getContent()))
    })

    instrumentSelected(selected)

    this.setTarget = function (transform) {
        instruments.forEach(instrument => {
            instrument.setTarget(transform)
            instrument.hide()
        })
        if (transform){
            instruments[selected].show()
        }
    }

    this.followTarget = function (){
        instruments.forEach(instrument => {
            instrument.followTarget()
        })
    }

    this.getContent = function() {
        return content
    }

    this.pauseMoving = function(value){
        instruments.forEach(instrument => {
            instrument.pauseMoving(value)
        })
    }

    this.enableCtrlMode = function (enable) {
        if (enable){
            if (!ctrlMode){
                instrumentSelected(1)
                ctrlMode = true
            }
        } else {
            ctrlMode = false
            instrumentSelected(manualSelected)
        }
    }

    this.enableAltMode = function (enable) {
        if (enable){
            if (!altMode){
                instrumentSelected(2)
                altMode = true
            }
        } else {
            altMode = false
            instrumentSelected(manualSelected)
        }
    }

    content.position = {
        x : 30,
        y : 25
    }
    layer.addChild(content)
    Creator.eventsManager.addEvent(Creator.events.OBJECT_UPDATED, this.followTarget, this)
}

module.exports.Instrumentation = Instrumentation