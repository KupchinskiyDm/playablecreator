const { Action, ActionType } = require("../../actions/action");
const { PropActionRecordGuard } = require("../../actions/guards/propActionRecordGuard");
const { ParticlesEmitter } = require("../../framework/ParticlesEmitter");

const Canvas = function (container) {
    const {Instrumentation} = require(path.resolve(__dirname, './Instrumentation'));
    const {TexturedSprite} = require(path.resolve(__dirname, '../../framework/TexturedSprite'));
    
    const self = this
    const content = document.createElement('div')
    let orient = null
    let workspaceLayer = null
    let serviseTopLayer = null
    let instruments = null
    let dragView = false
    let intialized = false
    let dragPoint = null
    let selected = null
    let focused = false
    let is_webgl = PIXI.utils.isWebGLSupported();

    window.is_webgl = is_webgl

    this.getIsViewDragging = function () {
        return dragView
    }

    const pixi = new PIXI.Application ({
        height : 1024,
        width : 1280,
        resolution: 1 ,
        backgroundColor : 0x2c3435
    })
    pixi.view.id = 'workspace'
    content.appendChild(pixi.view)

    const onSelect = function (target) {
        selected = target
        instruments.setTarget(target.getContent())
    }

    const removeSelectedItem = function() {
        Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_DELETED, selected.getName(), this)
        selected.remove()
        selected = null
        instruments.setTarget(null)
        instruments.pauseMoving(false)
    }

    const onSpriteDeletedViaHierarchy = function (event){
        if (selected) {
            removeSelectedItem()
        }
    }

    const onDropAsset = function (event) {
        if (dragPoint) {
            //TODO move asset type check to utils
            const dragItem = event.data.item
            if (dragItem.respath.indexOf('.png') !== -1 || dragItem.respath.indexOf('.jpg') !== -1){
                if (dragItem.respath.indexOf('s0und.png') === -1 && dragItem.respath.indexOf('unkn0wn.png') === -1){
                    console.log(dragItem);
                    const item = new TexturedSprite(dragItem, workspaceLayer, workspaceLayer.toLocal(dragPoint), onSelect)
                    selected = item
                    instruments.setTarget(item.getContent())
                }  
            }
        }
    }

    const onCreateParticlesEmiter = function (event) {
        const item = new ParticlesEmitter({}, workspaceLayer, {x: 0, y: 0}, onSelect)
        selected = item
        instruments.setTarget(item.getContent())
    }

    const makeGrid = function() {
        const gwidth = 240
        const gheight = 240
        const startx = -2880
        const starty = -1920
        for (let i = 0; i < 24; i ++){
            for (let j = 0; j < 16; j++) {
                const res = Creator.pixiLoader.get('grid.png')
                const cell = workspaceLayer.addChild(new PIXI.Sprite(res.texture))
                cell.interactive = true
                cell.position = {x : startx + i * gwidth , y : starty + j * gheight}
                cell.on("pointerup", (event) => {
                    if (!dragView){
                        selected = null
                        instruments.setTarget(null)
                        Creator.eventsManager.dispatchEvent(Creator.events.ALL_OBJECTS_UNSELECTED, null, this)
                    }
                    
                })
            }
        }
    }

    Creator.pixiLoader.setLoader(pixi.loader)

    const onStorageUpdated = function (event) {
        Creator.pixiLoader.load(event.data, ()=>{
            if (!intialized){
                pixi.renderer.backgroundColor = 0x404b4c
                pixi.stage.interactive = true
                pixi.stage.on('pointermove', (event) => { 
                    //TODO move to utils
                    let ox = event.data.originalEvent.x
                    let oy = event.data.originalEvent.y
                    if (ox >= content.offsetLeft && ox <= content.offsetLeft + content.offsetWidth) {
                        if (oy >= content.offsetTop && oy <= content.offsetTop + content.offsetHeight) {
                            dragPoint =  event.data.global
                        } else {
                            dragPoint = null
                        }
                    } else {
                        dragPoint = null
                    }
                })

                let center = {
                    x : content.offsetWidth / 2,
                    y : content.offsetHeight / 2
                }

                orient = new PIXI.Graphics()
                orient.beginFill(0x4c5252)
                orient.drawRect (0, 0, 1390, 640)
                orient.endFill()
 
                workspaceLayer = new PIXI.Container()
                workspaceLayer.sortableChildren = true

                makeGrid()

                workspaceLayer.addChild(orient)
                orient.position = {x : -1390/2, y : -640/2}
                
                pixi.stage.addChild(workspaceLayer)
                workspaceLayer.position = pixi.stage.toLocal(center)

                serviseTopLayer = new PIXI.Container()
                pixi.stage.addChild(serviseTopLayer)

                instruments = new Instrumentation(serviseTopLayer, self)

                workspaceLayer.scale = {
                    x : 0.7,
                    y : 0.7
                }

                if (content.addEventListener) {
                    content.addEventListener("wheel", (event) => {
                        const delta = Math.sign(event.deltaY) * 0.05;
                        if (delta < 0 && workspaceLayer.scale.x < 0.25){
                            return
                        }
                        if (delta > 0 && workspaceLayer.scale.x > 5){
                            return
                        }
                        workspaceLayer.scale = {
                            x : workspaceLayer.scale.x + delta,
                            y : workspaceLayer.scale.y + delta,
                        } 
                        instruments.followTarget()
                    })
                    content.addEventListener("mouseup", (event) => {
                        if (event.which == 2) {
                            dragView = false
                            document.body.style.cursor = "default";
                            instruments.pauseMoving(false)
                        }
                    })
                    content.addEventListener("mousedown", (event) => {
                        if (event.which == 2) {
                            dragView = true
                            document.body.style.cursor = "grab";
                            instruments.pauseMoving(true)
                        }
                    })

                    content.addEventListener("mouseout", (event) => {
                        if (event.which == 2) {
                            dragView = true
                            document.body.style.cursor = "grab";
                            instruments.pauseMoving(true)
                        }
                    })

                    content.addEventListener("mouseleave", (event) => {
                        focused = false
                        dragView = false
                        document.body.style.cursor = "default";
                        instruments.pauseMoving(false)
                    })
                    content.addEventListener("mousemove", (event) => {
                        focused = true
                        if (dragView) {
                            workspaceLayer.position = {
                                x : workspaceLayer.position.x + event.movementX,
                                y : workspaceLayer.position.y + event.movementY
                            }
                            instruments.followTarget()
                        }
                    })
                }

                window.addEventListener("keydown", event => {
                    if (selected && focused) {
                        const scontent = selected.getContent()
                        const SHIFT = 1
                        let npos = scontent.getGlobalPosition()

                        if (event.keyCode === 46) {
                            removeSelectedItem()
                        } 
    
                        if (event.keyCode === 37) {
                            npos = {
                                x : npos.x - SHIFT,
                                y : npos.y
                            }
                            Creator.actionManager.makeAction(new Action(selected, ActionType.SET_PROPERTY, { prop : 'position', value : scontent.parent.toLocal(npos) }), new PropActionRecordGuard())
                            instruments.followTarget()
                        } 
    
                        if (event.keyCode === 39) {
                            npos = {
                                x : npos.x + SHIFT,
                                y : npos.y
                            }
                            Creator.actionManager.makeAction(new Action(selected, ActionType.SET_PROPERTY, { prop : 'position', value : scontent.parent.toLocal(npos) }), new PropActionRecordGuard())
                            instruments.followTarget()
                        } 
    
                        if (event.keyCode === 38) {
                            npos = {
                                x : npos.x,
                                y : npos.y - SHIFT
                            }
                            Creator.actionManager.makeAction(new Action(selected, ActionType.SET_PROPERTY, { prop : 'position', value : scontent.parent.toLocal(npos) }), new PropActionRecordGuard())
                            instruments.followTarget()
                        } 
    
                        if (event.keyCode === 40) {
                            npos = {
                                x : npos.x,
                                y : npos.y + SHIFT
                            }
                            Creator.actionManager.makeAction(new Action(selected, ActionType.SET_PROPERTY, { prop : 'position', value : scontent.parent.toLocal(npos) }), new PropActionRecordGuard())
                            instruments.followTarget()
                        } 

                        if (event.keyCode === 17) {
                            instruments.enableCtrlMode(true)
                        } 

                        if (event.keyCode === 18) {
                            instruments.enableAltMode(true)
                        } 
                    }
                }) 

                window.addEventListener("keyup", event => {
                    if (selected && focused) {
                        if (event.keyCode === 17) {
                            instruments.enableCtrlMode(false)
                        } 
                        if (event.keyCode === 18) {
                            instruments.enableAltMode(false)
                        } 
                    }
                }) 

                Creator.eventsManager.addEvent(Creator.events.CREATE_PARTICLES_EMITTER, onCreateParticlesEmiter, this)
                Creator.eventsManager.addEvent(Creator.events.DROP_ASSET, onDropAsset, this)
                intialized = true
            }
        })
    }

    this.getContent = function (){
        return content
    }

    Creator.eventsManager.addEvent(Creator.events.UPDATED_ASSET_STORAGE, onStorageUpdated, this)
    Creator.eventsManager.addEvent(Creator.events.OBJECT_DELETED_FROM_HIERARCHY, onSpriteDeletedViaHierarchy, this)

    content.id = "canvas-container"
    container.appendChild(content)
}

module.exports.Canvas = Canvas