const TexturedSprite = function (config, parent, position, selectCallback) {

    const self = this
    let isSelected = false
    let texture = config.respath
    let name = config.resname
    let cachetexture = texture
    let tint = null;
    let targetBorderRect = null
    
    const content = parent.addChild(new PIXI.Sprite(Creator.pixiLoader.get(texture).texture))
    content.anchor.set(0.5)
    content.interactive = true
    content.position = position ? position : {x :0, y : 0}
    content.wrapper = this;

    const removeTargetBorderRect = function () {
        if (targetBorderRect) {
            parent.removeChild(targetBorderRect)
            targetBorderRect = null
        }
    }

    const updateTargetBorderRect = function () {
        if (!isSelected) {
            return
        }
        if (!targetBorderRect){
            targetBorderRect = new PIXI.Graphics()
            parent.addChild(targetBorderRect)
        }
        const borderWidht = 0
        const borderGap = 0

        targetBorderRect.clear()
        targetBorderRect.lineStyle(3, 0xccbe99)
        targetBorderRect.drawRect(0, 0, content.width + borderWidht, content.height + borderWidht )

        targetBorderRect.pivot = {x : content.anchor.x * content.width - borderGap / 2, y : content.anchor.y * content.height - borderGap / 2}
        targetBorderRect.rotation = content.rotation
        let dx =  content.position.x
        let dy =  content.position.y
        targetBorderRect.position = {x : dx, y : dy}
        targetBorderRect.skew = content.skew
        targetBorderRect.zIndex = content.zIndex - 1
    }

    const onCleanStorage = function (event) {
        content.texture = Creator.pixiLoader.get('image.png').texture
    }

    const onStorageUpdated = function (event) {
        const list = event.data
        list.forEach(element => {
            if (Creator.utils.getFormatedTexturePath(element) === Creator.utils.getFormatedTexturePath(cachetexture)){
                content.texture = Creator.pixiLoader.get(element).texture
                cachetexture = element
                updateTargetBorderRect()
            }
        });
    }

    content.on("mousedown", (event) => {
        if (event.data.originalEvent.which !== 2){
            Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_SELECTED, this, this)
        }
    })

    const onAllUnselected = function (event) {
        removeTargetBorderRect()
    }

    const onSpriteSelected = function (event){
        if (event.data === this){
            isSelected = true
            updateTargetBorderRect()
            if (selectCallback){
                selectCallback(this)
            }
        } else {
            isSelected = false
            removeTargetBorderRect()
        }
    }

    this.getAllowedResExtensions = function () {
        return ['png', 'jpg']
    }

    this.getResPathFormatted = function () {
        return Creator.utils.getFormatedTexturePath(texture)
    }

    this.getResPath = function () {
        return texture
    }

    this.setResPath = function (path) {
        texture = path
        cachetexture = texture
        content.texture = Creator.pixiLoader.get(texture).texture
        Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_UPDATED, self, self)
        updateTargetBorderRect()
        // TODO make this code great to parse spine, spritesheets and other
    }

    this.getCachedTexture = function () {
        return cachetexture
    }

    this.getContent = function() {
        return content
    }

    this.getName = function () {
        return name
    }

    this.setName = function (value) {
        name = value
        Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_UPDATED, self, self)
    }

    this.setOrder = function (value) {
        content.zIndex = value
        updateTargetBorderRect()
    }

    this.setProperty = function (prop, value) {
        content[prop] = value;
        Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_UPDATED, self, self)
        updateTargetBorderRect()
    }

    this.getProperty = function (prop) {
        return content[prop];
    }

    this.setTint = function (value) {
        tint = value
        content.tint = Number('0x' + ((1 << 24) + (tint.rgb[0] << 16) + (tint.rgb[1] << 8) + tint.rgb[2]).toString(16).slice(1));
        Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_UPDATED, self, self)
    }

    this.getTint = function () {
        return tint
    }

    this.getOrder = function () {
        return  content.zIndex
    }

    this.remove = function(){
        parent.removeChild(content)
        if (targetBorderRect){
            parent.removeChild(targetBorderRect)
        }
        Creator.eventsManager.removeEvent(Creator.events.START_UPDATE_ASSET_STORAGE, onCleanStorage, this)
        Creator.eventsManager.removeEvent(Creator.events.PIXI_LOADER_READY, onStorageUpdated, this)
    }

    this.nodeType = 'Sprite'

    Creator.eventsManager.addEvent(Creator.events.START_UPDATE_ASSET_STORAGE, onCleanStorage, this)
    Creator.eventsManager.addEvent(Creator.events.PIXI_LOADER_READY, onStorageUpdated, this)
    Creator.eventsManager.addEvent(Creator.events.OBJECT_SELECTED, onSpriteSelected, this)
    Creator.eventsManager.addEvent(Creator.events.ALL_OBJECTS_UNSELECTED, onAllUnselected, this)
    
    Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_CREATED, this, this)
    Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_SELECTED, this, this)
}

module.exports.TexturedSprite = TexturedSprite