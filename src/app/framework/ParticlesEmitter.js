const ParticlesEmitter = function (config, parent, position, selectCallback) {

    const self = this
    let isSelected = false
    let texture = 'particle.png'
    let name = 'particles_emitter'
    let cachetexture = texture
    let tint = null;
    let targetBorderRect = null
    let elapsed = Date.now()
    let emitter
    
    const emitterContainer = parent.addChild(new PIXI.Container())
    emitterContainer.position = {x :0, y : 0}
    emitterContainer.interactive = false

    const content = parent.addChild(new PIXI.Container())
    content.interactive = true
    content.position = position ? position : {x :0, y : 0}
    content.wrapper = this

    let data = Creator.model.getInitialParticleEmitterData()
   
    const updateEmitter = function(){
        requestAnimationFrame(updateEmitter)
        if(emitter){
            var now = Date.now()
            emitter.updateSpawnPos(content.position.x, content.position.y)
            emitter.update((now - elapsed) * 0.001)
        }
        elapsed = now
    }

    updateEmitter();

    const recreateEmitter = function(config) {
        if (emitter) {
            emitter.emit = false;
            emitter.destroy();
            emitterContainer.removeChild(emitter)
            emitter = null;
            
        }
        const t = Creator.pixiLoader.get(texture).texture
        emitter = new PIXI.particles.Emitter(emitterContainer, t, config)
        emitter.maxSpeed = config.maxSpeed
        emitter.emit = true;       
    } 

    const removeTargetBorderRect = function () {
        if (targetBorderRect) {
            parent.removeChild(targetBorderRect)
            targetBorderRect = null
        }
    }

    const updateTargetBorderRect = function () {
        if (!isSelected) {
            return
        }
        if (!targetBorderRect){
            targetBorderRect = new PIXI.Graphics()
            parent.addChild(targetBorderRect)
        }

        const borderGap = 0

        targetBorderRect.clear()
        targetBorderRect.lineStyle(3, 0xccbe99)
        targetBorderRect.drawRect(-50, -50, 100, 100 )
        targetBorderRect.rotation = content.rotation

        let dx =  content.position.x
        let dy =  content.position.y
        targetBorderRect.position = {x : dx, y : dy}
        targetBorderRect.skew = content.skew
        targetBorderRect.zIndex = content.zIndex - 1
    }

    const onCleanStorage = function (event) {
        content.texture = Creator.pixiLoader.get('particle.png').texture
    }

    const onStorageUpdated = function (event) {
        const list = event.data
        list.forEach(element => {
            if (Creator.utils.getFormatedTexturePath(element) === Creator.utils.getFormatedTexturePath(cachetexture)){
                content.texture = Creator.pixiLoader.get(element).texture
                cachetexture = element
                updateTargetBorderRect()
            }
        });
    }

    content.on("mousedown", (event) => {
        if (event.data.originalEvent.which !== 2){
            Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_SELECTED, this, this)
        }
    })

    const onAllUnselected = function (event) {
        removeTargetBorderRect()
    }

    const onSpriteSelected = function (event){
        if (event.data === this){
            isSelected = true
            updateTargetBorderRect()
            if (selectCallback){
                selectCallback(this)
            }
        } else {
            isSelected = false
            removeTargetBorderRect()
        }
    }

    this.getAllowedResExtensions = function () {
        return ['png', 'jpg']
    }

    this.getResPathFormatted = function () {
        return Creator.utils.getFormatedTexturePath(texture)
    }

    this.getResPath = function () {
        return texture
    }

    this.setResPath = function (path) {
        texture = path
        cachetexture = texture
        recreateEmitter(data)
        Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_UPDATED, self, self)
        updateTargetBorderRect()
        // TODO make this code great to parse spine, spritesheets and other
    }

    this.getCachedTexture = function () {
        return cachetexture
    }

    this.getContent = function() {
        return content
    }

    this.getName = function () {
        return name
    }

    this.setName = function (value) {
        name = value
        Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_UPDATED, self, self)
    }

    this.setOrder = function (value) {
        content.zIndex = value
        emitterContainer.zIndex = value + 1
        updateTargetBorderRect()
    }

    const getEmitterDataPropPath = function (prop) {
        if (prop.indexOf('emitterData.') !== -1){
            return prop.split('emitterData.').pop()
        }
        return null
    }

    const getEmmiterProperty = function (prop) {
        if (prop.indexOf('.') !== -1) {
            let node = data;
            const fullPath = prop.split('.')
            const lastInPath = fullPath.pop()
            fullPath.forEach(part => node = node[part])
            return node[lastInPath]
        } else {
            return data[prop]
        }
    }

    const setEmmiterProperty = function (prop, value) {
        if (!emitter) return
        if (prop.indexOf('.') !== -1) {
            let node = data;
            const fullPath = prop.split('.')
            const lastInPath = fullPath.pop()
            fullPath.forEach(part => node = node[part])
            node[lastInPath] = value
        } else {
            data[prop] = value
        }
        recreateEmitter(data)
    }

    this.setProperty = function (prop, value) {
        const emitterProp = getEmitterDataPropPath(prop)
        if (emitterProp) {
            setEmmiterProperty(emitterProp, value)
        } else {
            // for default props
            switch (prop) {
                case 'blendMode' : {
                    setEmmiterProperty('blendMode', PIXI.BLEND_MODES[value] || PIXI.BLEND_MODES[0])
                    break
                }
                case 'position' : {
                    content[prop] = value;
                }
                case 'emit' : {
                    emitter && (emitter.emit = value)
                }
                default : {
                    break
                }
            }
        }
        
        Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_UPDATED, self, self)
        updateTargetBorderRect()
    }

    this.getProperty = function (prop) {
        const emitterProp = getEmitterDataPropPath(prop)
        if (emitterProp) {
            return getEmmiterProperty(emitterProp)
        } 
        
        // for default props
        switch (prop) {
            case 'blendMode' : {
                return PIXI.BLEND_MODES[getEmmiterProperty('blendMode').toUpperCase() || PIXI.BLEND_MODES.NORMAL]
            }
            case 'position' : {
                return content[prop];
            }
            case 'emit' : {
                return emitter && emitter.emit
            }
            default : {
                return emitterContainer[prop];
            }
        }
    }

    this.getData = function(){
        return data;
    }

    this.setTint = function (value) {
        tint = value
        content.tint = Number('0x' + ((1 << 24) + (tint.rgb[0] << 16) + (tint.rgb[1] << 8) + tint.rgb[2]).toString(16).slice(1));
        Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_UPDATED, self, self)
    }

    this.getTint = function () {
        return tint
    }

    this.getOrder = function () {
        return  content.zIndex
    }

    this.save = function(){
        Creator.eventsManager.dispatchEvent(Creator.events.EXPORT_PARTICLES_EMITTER, self, self)
    }

    this.import = function(){
        Creator.eventsManager.dispatchEvent(Creator.events.IMPORT_PARTICLES_EMITTER, self, self)
    }

    this.reset = function(){
        data = Creator.model.getInitialParticleEmitterData()
        texture = 'particle.png'
        cachetexture = texture
        content.texture = Creator.pixiLoader.get(texture).texture
        self.setNewData (data);
    }

    this.play = function(){
        recreateEmitter(data)
    }

    this.stop = function(){
        emitter.emit = false;
    }

    this.setNewData = function(newData){
        data = newData
        try {
            recreateEmitter(data)
        } catch (error) {
            console.error(error)
            this.reset()
        }
        
        Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_UPDATED, self, self)
    }

    this.remove = function(){
        parent.removeChild(content)
        parent.removeChild(emitterContainer)
        
        if (emitter) {
            emitter.emit = false;
            emitter.destroy();
            parent.removeChild(emitter)
            emitter = null;
            
        }
        if (targetBorderRect){
            parent.removeChild(targetBorderRect)
        }
        Creator.eventsManager.removeEvent(Creator.events.START_UPDATE_ASSET_STORAGE, onCleanStorage, this)
        Creator.eventsManager.removeEvent(Creator.events.PIXI_LOADER_READY, onStorageUpdated, this)
    }

    this.nodeType = 'ParticlesEmitter'

    recreateEmitter(data);

    Creator.eventsManager.addEvent(Creator.events.START_UPDATE_ASSET_STORAGE, onCleanStorage, this)
    Creator.eventsManager.addEvent(Creator.events.PIXI_LOADER_READY, onStorageUpdated, this)
    Creator.eventsManager.addEvent(Creator.events.OBJECT_SELECTED, onSpriteSelected, this)
    Creator.eventsManager.addEvent(Creator.events.ALL_OBJECTS_UNSELECTED, onAllUnselected, this)
    
    Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_CREATED, this, this)
    Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_SELECTED, this, this)
}

module.exports.ParticlesEmitter = ParticlesEmitter