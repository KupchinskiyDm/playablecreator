const PropActionRecordGuard = function (forseDiscard = false, forcedRecord = false) {
    this.check = function (action) {
        if (forseDiscard && !forcedRecord) {
            return false
        }
        const targetProp = action.target.getProperty(action.config.prop)
        const valueProp = action.config.value
        if (typeof targetProp === 'object') {
            if (targetProp.hasOwnProperty('_x') && targetProp.hasOwnProperty('_y')) {
                return targetProp.x !== valueProp.x || targetProp.y !== valueProp.y || forcedRecord
            }
        } else {
            if (valueProp !== targetProp || forcedRecord) {
                return true
            }
        }
        return false
    }
}

module.exports.PropActionRecordGuard = PropActionRecordGuard