const SpriteNameActionGuard = function () {
    this.check = function (action) {
        const targetProp = action.target.getName()
        const valueProp = action.config.value
        return valueProp !== targetProp
    }
}

module.exports.SpriteNameActionGuard = SpriteNameActionGuard