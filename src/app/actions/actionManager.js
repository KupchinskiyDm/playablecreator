const ActionManager = function () {
    const self = this
    this.currentHistoryPosition = -1
    this.history = []

    this.makeAction = (action, recordGuard = {check : () => true}) => {
        if (recordGuard.check(action)){
            if (self.currentHistoryPosition === 0 && self.history.length > 1){
                self.history = []
            } else if (self.currentHistoryPosition < self.history.length - 1 ){
                self.history = self.history.slice(0, self.currentHistoryPosition + 1)
            }
            self.history.push(action)
            self.currentHistoryPosition = self.history.length -1
        }
        action.execute()
    }

    this.undo = () => {
        if (self.history.length && self.currentHistoryPosition >= 0){
            Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_SELECTED, self.history[self.currentHistoryPosition].target, this)
            self.history[self.currentHistoryPosition].execute(false, true)
            if (self.currentHistoryPosition > 0) self.currentHistoryPosition --
        }
    }

    this.redo = () => {
        if (self.history.length){
            Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_SELECTED, self.history[self.currentHistoryPosition].target, this)
            self.history[self.currentHistoryPosition].execute(false)
            if (self.currentHistoryPosition < self.history.length -1) self.currentHistoryPosition ++
        }
    }

    return this
}

module.exports.ActionManager = (new ActionManager())