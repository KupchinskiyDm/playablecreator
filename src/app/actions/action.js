const ActionType = {
    SET_PROPERTY : 'SET_PROPERTY',
    SET_NAME : 'SET_NAME',
    SET_RES_PATH : 'SET_RES_PATH',
    SET_TINT : 'SET_TINT',
    OBJECT_CREATE : 'OBJECT_CREATE',
    OBJECT_DELETE : 'OBJECT_DELETE'
}

const Action = function (target, actionType, config, previousOverride = null) {
    this.target = target
    this.actionType = actionType
    this.config = config
    this.previous = null
    this.execute = (rewiteState = true, usePrevious = false ) => {
        const applyConfig = usePrevious && this.previous ? this.previous : config
        switch (this.actionType) {
            case ActionType.SET_PROPERTY:
                if (this.target) {
                    if (rewiteState) {
                        const targetProp = this.target.getProperty(config.prop)
                        let value = null
                        if (typeof targetProp === 'object') {
                            if (targetProp.hasOwnProperty('_x') && targetProp.hasOwnProperty('_y')) {
                                value = {
                                    x : targetProp.x,
                                    y : targetProp.y
                                }
                            }
                        } else {
                            value = targetProp
                        }
                        this.previous = previousOverride ? previousOverride : {prop : config.prop, value}
                    }
                    this.target.setProperty(applyConfig.prop, applyConfig.value)
                }
                break;
            case ActionType.SET_NAME:
                if (this.target) {
                    if (rewiteState) {
                        this.previous = {value : this.target.getName()}
                    }
                    this.target.setName(applyConfig.value)
                }
                break;
        
            default:
                console.error('Unknown action:' + actionType)
                break;
        }
    }
}

module.exports.ActionType = ActionType
module.exports.Action = Action