

const FileProcessor = (function () {
    const fs = require('fs');
    const rimraf = require("rimraf");
    const moment = require('moment');
    const walker = require('walker');
    const self = this

    let op_callback = null
    let cacheSeparator = ''

    this.openFolder = function (longpath, callback) {
        cacheSeparator = `${longpath.replace(/\\/g, '/')}`
        startUpdateFlow(longpath, callback)
    }

    this.getAppCachePath = function (){
        return `${path.resolve(Creator.electronApp.app.getPath('userData'), '_temp')}/`
    }

    this.saveToFile = function(filePath, data, callback){
        fs.writeFile(filePath, data, callback ? callback : (error) => {
            if (error) {
                console.error('File saving failed. ')
                console.log(error)
            }
        });
    }    

    this.fromFile = function(filePath, callback){
        fs.readFile(filePath, callback ? callback : (error, data) => {
            if (error) {
                console.error('File reading failed. ')
                console.log(error)
            }
        });
    }    

    let updateFSTimeout = -1

    function startUpdateFlow (longpath, callback) {
        Creator.eventsManager.dispatchEvent(Creator.events.START_UPDATE_ASSET_STORAGE, null, self)
        deleteCache(()=>{
            makeAssetsTree(longpath, (dirs)=>{
                readAssets(dirs, [], (out)=>{
                    cacheFile(out, [], (out)=>{
                        Creator.eventsManager.dispatchEvent(Creator.events.UPDATED_ASSET_STORAGE, out, self)
                    })
                })
            })
        })
        fs.watch(longpath, function (event, filename) {
            if (filename) {
                if (updateFSTimeout > -1){
                    clearTimeout(updateFSTimeout)
                }
                updateFSTimeout = setTimeout(()=>{
                    startUpdateFlow(longpath, callback)
                }, 1000)
            }
        })
    }

    function mkDir (prefix, dirs, callback){
        if (!dirs.length){
            callback()
        }
        let folder = dirs.shift()
        let pref = `${prefix}/${folder}`
        fs.mkdir(pref, 0777, function(err) {
            if (dirs.length){
                mkDir(pref, dirs, callback)
            } else {
                callback()
            }
        });
    }

    function cacheFile (files, out, callback){
        let longfile = files.pop()
        let cachepath = `${path.resolve(Creator.electronApp.app.getPath('userData'), '_temp')}`

        let shortfile = longfile.split(cacheSeparator)[1]
        let fname =''
        let fprefix = ''
        let fuid = ''

        if( shortfile.split('/').length > 1 ){
            fname = shortfile.split('/')[shortfile.split('/').length -1]
            let topop = shortfile.split('/')
            topop.pop()
            fprefix = topop.join('/')
            fuid = `${fprefix}/${Creator.utils.uid()}@${fname}`
        } else{
            fuid = `${Creator.utils.uid()}@${shortfile}`
        }
        
        let cachedfile = `${cachepath}${fuid}`
        let parsed = fuid.split('/')
        let dirstocreate = []
        if (parsed.length > 1){
            parsed.pop()
            dirstocreate = parsed
        }
        mkDir(cachepath, dirstocreate, ()=>{
            fs.copyFile(longfile, cachedfile, (err) => {
                if (files.length){
                    out.push(cachedfile)
                    cacheFile(files, out, callback)
                } else {
                    callback (out)
                }
            });
        })
    }

    function readAssets (dirs, out, callback){
        const dest = dirs.pop()
        fs.readdir(dest, (err, files) => {
            files.forEach(file => {
                if (file.indexOf('.') !== -1){
                    out.push(`${dest}/${file}`)
                }
            });
            if (dirs.length){
                readAssets(dirs, out, callback)
            } else {
                callback(out)
            }
        });
    }

    function deleteCache (callback){
        const cachepath = path.resolve(Creator.electronApp.app.getPath('userData'), '_temp')
        if(fs.existsSync(cachepath)) {
            rimraf(cachepath, function () { 
                fs.mkdir(cachepath, 0777, function(err) {
                    callback()
                });
            });
        } else {
            fs.mkdir(cachepath, 0777, function(err) {
                callback()
            });
        }
    }

    function makeAssetsTree(longpath, callback) { 
        let directories = []
        directories.push(`${longpath.replace(/\\/g, '/')}`)

        walker(longpath).on('dir', function(dir, stat) {

            if (dir === longpath) return;
            directories.push(`${dir.replace(/\\/g, '/')}`)

        }).on('end', function() {

            callback(directories);

        })
    };

    return this;
})();

exports.FileProcessor = FileProcessor;