// global imports

// Extensions
Object.defineProperty(String.prototype, 'capitalize', {
    value: function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    },
    enumerable: false
});

Object.defineProperty(String.prototype, 'splitCamelCase', {
    value: function(splitSymbol = ' ') {
        return this.match(/[A-Z][a-z]+|[0-9]+/g).join(splitSymbol);
    },
    enumerable: false
});
// ---------

const path = require('path');
const { remote } = require('electron');
const { dialog } = remote
const { AppInspector } = require(path.resolve(__dirname, './app/appInspector'))

const APP_VERSION = remote.app.getVersion()
console.log('App version ' + APP_VERSION)

const Creator = {
    appVersion : APP_VERSION,
    eventsManager : require(path.resolve(__dirname, './app/events/EventManager')).EventManager,
    events : require(path.resolve(__dirname, './app/events/events')),
    dialog,
    electronApp : remote,
    model : require(path.resolve(__dirname, './app/models')).AppModel,
    drag : require(path.resolve(__dirname, './app/ui/DragItem')).DragItem,
}

// Creator.electronApp.getCurrentWindow().openDevTools()

Creator.fileProcessor = require(path.resolve(__dirname, './app/fileprocessor')).FileProcessor
Creator.utils = require(path.resolve(__dirname, './app/utils')).Utils
Creator.pixiLoader = require(path.resolve(__dirname, './app/canvas/loader')).PIXIResLoader
Creator.titleBar = new require(path.resolve(__dirname, './app/ui/titlebar')).TitleBar()
Creator.menu = new require(path.resolve(__dirname, './app/ui/menu')).Menu()
Creator.inspector = new AppInspector()
Creator.actionManager = require(path.resolve(__dirname, './app/actions/actionManager')).ActionManager


let ctrlKey = false
let shiftKey = false

window.Creator = Creator

window.addEventListener("keydown", event => {
   
    switch (event.keyCode) {
        case 91: // ctrl
            event.preventDefault()
            ctrlKey = true
            break;
        case 16: // shift
            event.preventDefault()
            shiftKey = true
            break;
        default:
            break;
    }
    if (process.platform === 'darwin') {
        if (!shiftKey && ctrlKey && event.keyCode === 90){
            event.preventDefault()
            Creator.actionManager.undo()
        }
        if (ctrlKey && shiftKey && event.keyCode === 90){
            event.preventDefault()
            Creator.actionManager.redo()
        }
    }

    if (event.keyCode === 123) {
        Creator.electronApp.getCurrentWindow().openDevTools()
    } else if (event.which === 116) {
        location.reload();
    }
    if (event.keyCode === 46) {
        Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_DELETED_FROM_HIERARCHY, {}, this)
    } 
}) 


window.addEventListener("keyup", event => {
    event.preventDefault()
    switch (event.keyCode) {
        case 91: // ctrl
            ctrlKey = false
            break;
        case 16: // ctrl
            shiftKey = false
            break;
        default:
            break;
    }
}) 

const onOpennProjectDlgHandler = function (event) {
    Creator.dialog.showOpenDialog(
        Creator.electronApp.getCurrentWindow(),{properties : ['openDirectory']}
    ).then(result => {
        if (!result.canceled){
            console.log(Creator.fileProcessor.getAppCachePath())
            Creator.fileProcessor.openFolder(result.filePaths[0], (results)=>{
                //inspector.onOpenProjectFolder(results)
            })
        }
    }).catch(err => {
        console.log(err)
    })
}

const onSaveParticleDlgHandler = function (event) {
    const raw = event.data.getData()
    raw.entityType = "generated_particle_emitter-" + Creator.appVersion
    const particleData = JSON.stringify(raw)
    Creator.dialog.showSaveDialog(Creator.electronApp.getCurrentWindow(), {
        filters:[
            {
                name: 'json',
                extensions: ['json']
            }
        ]})
        .then(result => {
            if (!result.canceled){
                
                Creator.fileProcessor.saveToFile(result.filePath, particleData, () => {
                    // console.log('File saved :')
                    // console.log(result.filePath)
                } )
            }
    }).catch(err => {
        console.log(err)
    })
}

const onImportParticleDlgHandler = function (event) {
    const particle = event.data
    Creator.dialog.showOpenDialog(
        Creator.electronApp.getCurrentWindow(),{
            properties : ['openFile'],
            filters:[
                {
                    name: 'json',
                    extensions: ['json']
                }
            ]
        }
    ).then(result => {
        if (!result.canceled){
            Creator.fileProcessor.fromFile(result.filePaths[0], (error, data) => {
                if (error){
                    console.error('Cannot open file')
                    return;
                }
                try {
                    const parsed = JSON.parse(data);
                    if (!parsed || !parsed.entityType){
                        throw new Error('Particles entity type not found')
                    }
                    const splited = parsed.entityType.split('-')
                    const fileVersion = splited && splited.length > 1 ? splited[1] : '0.0.0'
                    if (fileVersion !== Creator.appVersion){
                        Creator.dialog.showMessageBox(Creator.electronApp.getCurrentWindow(),{
                            message : 'Particle created with another app version.', 
                            type : 'warning',
                            buttons : ['Import anyway', 'Cancel'],
                            cancelId : 1
                        }).then(result => {
                            if (result.response === 0){
                                particle.setNewData(parsed)
                            }
                        })
                    } else {
                        particle.setNewData(parsed)
                    }
                } catch (err) {
                    console.error('Unsupported particle structure type')
                    Creator.dialog.showMessageBoxSync(Creator.electronApp.getCurrentWindow(),{message : err.message, type : 'error'})
                }
            } )
        }
    }).catch(err => {
        console.log(err)
    })
}

Creator.eventsManager.addEvent(Creator.events.OPEN_PROJECT_DLG, onOpennProjectDlgHandler, this)
Creator.eventsManager.addEvent(Creator.events.EXPORT_PARTICLES_EMITTER, onSaveParticleDlgHandler, this)
Creator.eventsManager.addEvent(Creator.events.IMPORT_PARTICLES_EMITTER, onImportParticleDlgHandler, this)