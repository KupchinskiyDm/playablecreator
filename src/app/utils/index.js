const Utils = (function (){

    this.addButton = function (calssName, img, parent, callback){
        const button =  document.createElement('div')
        button.className = calssName
        const image = document.createElement('img')
        image.className = `${calssName}-image`
        image.src = img
        if (callback){
            button.addEventListener('mouseup', callback, false);
        }
        button.appendChild(image)
        parent.appendChild(button)
        return button
    }
    
    this.uid = function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }
        let val = s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4();
        return val;
    };

    this.getFormatedTexturePath = function (source) {
        const relative = source.split(Creator.fileProcessor.getAppCachePath())[1]
        if (!relative){
            return source
        }
        const prefix = relative.split('@')[0].split('/')
        let formatted = ''
        if (prefix.length > 1){
            prefix.pop()
            formatted =`${prefix.join('/')}/${relative.split('@')[1]}`
        } else {
            formatted = relative.split('@')[1]
        }
        return formatted
    }

    this.moveInArray = function (arr, from, to) {
        let cutOut = arr.splice(from, 1) [0];
        arr.splice(to, 0, cutOut);
    }

    this.checkFloatMatch = function (input) {
        const parsed = Number.parseFloat(input)
        return !Number.isNaN(parsed)
    }

    this.checkMinMax = function (min, max, input) {
        const parsed = Number.parseFloat(input)
        if (!Number.isNaN(parsed)){
            if (parsed >= min && parsed <= max){
                return true
            }
        }
        return false
    }

    this.getAllElementsFromPoint = function (x, y){
        var elements = [];
        var display = [];
        var item = document.elementFromPoint(x, y);
        var lastItem;
        while (item && item !== lastItem && item !== document.body && item !== document && item !== document.documentElement) {
            elements.push(item);
            display.push(item.style.display);
            item.style.display = "none";
            lastItem = item;
            item = document.elementFromPoint(x, y);
        }
        for (var i = 0; i < elements.length; i++) {
            elements[i].style.display = display[i];
        }
        return elements;
    }
    
    return this
})()

module.exports.Utils = Utils