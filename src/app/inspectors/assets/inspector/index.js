const Assets = function (container) {
    const self = this
    const content = document.createElement('div')
    const caption = document.createElement('assets-caption')
    const image = document.createElement('img')
    const captiontext = document.createTextNode('Assets')
    const vertscrollnode = document.createElement('assets-vert-scrollnode')
    let resFieldFocused = null;
    let mousedown = false
    let assets = {}
    let selected = null
    let dragitem = null
    // public

    // private
    const onFocusHandle = function(target) {
        if (!selected || selected.getContent().id !== target ){
            if (assets.hasOwnProperty(target)){
                for (let key in assets){
                    if (key !== target){
                        assets[key].setSelected(false)
                    }
                }
                selected = assets[target]
                selected.setSelected(true)
            }   
        }
        return selected
    }

    const onMouseDrag = function (event) {
        if (dragitem){
            if (!mousedown) {
                dropDragItem(null)
            } else {
                moveAt(event.pageX, event.pageY);
                /*
                var x = event.clientX;
                var y = event.clientY;
                targets = Creator.utils.getAllElementsFromPoint(event.clientX, event.clientY);

                if (target.id === 'res-path-field'){
                    resFieldFocused = target
                    console.log(resFieldFocused)
                }
                */
            }
        }
    }

    function moveAt(pageX, pageY) {
        dragitem.style.left = pageX - dragitem.offsetWidth / 2 + 'px';
        dragitem.style.top = pageY - dragitem.offsetHeight / 2 + 'px';
    }
    
    const dropDragItem = function (event) {
        selected = null
        if (dragitem){
            dragitem.removeEventListener('mouseup', dropDragItem);
            document.body.removeChild(dragitem);
            document.removeEventListener('mousemove', onMouseDrag)
            Creator.eventsManager.dispatchEvent(Creator.events.DROP_ASSET, {item : dragitem, event}, self)
            dragitem = null
            Creator.objectResPathSetting && Creator.objectResPathSetting.setHighlight(false);
        }
    }

    const onMouseUp = function (event) {
        mousedown = false
    }

    const clickHandler = function (event) {
        if (event && event.which == 2) {
            event.preventDefault();
            return
        }
        mousedown = true
        dropDragItem(null)
        let focused = onFocusHandle(event.target.id)
        if (focused) {
            dragitem = focused.getContent().cloneNode(true); // true means clone all childNodes and all event handlers
            dragitem.id = `${focused.getContent().id}_drag`
            dragitem.style.position = 'absolute'
            dragitem.style.zIndex = 10000
            dragitem['respath'] = focused.getPath()
            dragitem['resname'] = focused.getName()
            dragitem.addEventListener('mouseup', dropDragItem, false);
            document.body.appendChild(dragitem)
            document.addEventListener('mousemove', onMouseDrag)
            Creator.objectResPathSetting && Creator.objectResPathSetting.setHighlight(true);
        }
    }
    const cleanStorage = function (event){
        document.body.style.cursor = "progress";
        for (let key in assets){
            vertscrollnode.removeChild(assets[key].getContent())
        }
        assets = {}
    }

    const onStorageUpdated = function (event) {
        document.body.style.cursor = "default";
        const list = event.data
        let prefix = 0
        list.forEach(element => {
            let ItemClass = require(path.resolve(__dirname, '../item')).AssetItem
            let item = new ItemClass(vertscrollnode, element, prefix)
            prefix ++
            assets[item.getContent().id] = item
        });
    }

    if (vertscrollnode.addEventListener) {
        vertscrollnode.addEventListener('mousedown', clickHandler, false)
        vertscrollnode.addEventListener('mouseup', onMouseUp, false)
        
    }

    // ids
    vertscrollnode.className = 'assets-vert-scrollnode'
    image.src = 'res/assets.png'
    image.className = 'inspector-caption-image'
    caption.className = 'asset-inspector-caption'
    content.id = "assets"

    // appends (order is matter!)

    caption.appendChild(image)
    caption.appendChild(captiontext)
    content.appendChild(caption)
    content.appendChild(vertscrollnode)
    container.appendChild(content)

    Creator.eventsManager.addEvent(Creator.events.UPDATED_ASSET_STORAGE, onStorageUpdated, this)
    Creator.eventsManager.addEvent(Creator.events.START_UPDATE_ASSET_STORAGE, cleanStorage, this)
    
}

module.exports.Assets = Assets