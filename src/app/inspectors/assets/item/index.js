const AssetItem = function (container, config, prefix) {
    let splitted = config.split('/')[config.split('/').length -1]
    let name = splitted.split('@')[1]
    let longname = splitted.split('@')[1]
    let path = ''

    if (name.indexOf('.png') !== -1 || name.indexOf('.jpg') !== -1){
        path = config
    } else if (name.indexOf('.mp3') !== -1){
        path = 'res/s0und.png'
    } else {
        path = 'res/unkn0wn.png'
    }

    //TODO move to utils

    let namearr = name.split('')
    if (namearr.length > 15){
        name = `${namearr.slice(0,12).join('')}...`
    }
    
    const content = document.createElement('assets-caption')
    const title = document.createElement('span')
    const imgcontainer = document.createElement('div')
    const img = document.createElement('img')

    title.innerText = name
    img.src = path

    imgcontainer.className = 'assets-item-image-container'
    img.className = 'assets-item-image'
    title.className = 'assets-item-caption'
    content.className = 'assets-item'
    content.setAttribute('title', Creator.utils.getFormatedTexturePath(config) );
    content.id = `${path}_${prefix}`

    imgcontainer.appendChild(img)
    content.appendChild(imgcontainer)
    content.appendChild(title)

    container.appendChild(content)

    this.setSelected = function (value){
        imgcontainer.className = value ? 'assets-item-image-container selected' : 'assets-item-image-container'
    }

    this.getContent = function() {
        return content
    }

    this.getPath = function() {
        return path
    }

    this.getName = function() {
        return namearr.join('')
    }
}

module.exports.AssetItem = AssetItem