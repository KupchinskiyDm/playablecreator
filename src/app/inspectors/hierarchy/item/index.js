const HierarchyItem = function (container, sprite, name, order, vault) {

    const content = document.createElement('hierarchy-caption')
    const title = document.createElement('span')
    const self = this
    let _index = order.length
    let _order = order
    let _vault = vault
    let isSelected = false

    content.className = 'hierarchy-item'
    title.innerText = name
    sprite.setName(name)
    
    if (content.addEventListener){
        content.addEventListener('mousedown',()=>{
            Creator.eventsManager.dispatchEvent(Creator.events.OBJECT_SELECTED, sprite, this)
            Creator.drag.setItem(this)
        })

        content.addEventListener('mouseup',()=>{
            let item = Creator.drag.getItem()
            if (item && item !== this) {
                content.className = 'hierarchy-item'
            }
            Creator.drag.setItem(null)
        })

        content.addEventListener('mousemove',()=>{
            let item = Creator.drag.getItem()
            if (item && item !== this) {
                content.className = 'hierarchy-item drag'
            }
        })

        content.addEventListener('mouseleave',()=>{
            let item = Creator.drag.getItem()
            if (item && item !== this) {
                content.className = 'hierarchy-item'
            }
        })
    }

    const onObjectSelected = function (event){
        content.className = event.data === sprite ? 'hierarchy-item active' : 'hierarchy-item'
    }

    const onObjectUnselected = function () {
        content.className = 'hierarchy-item'
    }

    content.appendChild(title)
    container.appendChild(content)

    this.getContent = function() {
        return content
    }

    this.getName = function () {
        return name
    }

    const updateName = function (value) {
        delete _vault[name]
        name = value
        title.innerText = name
        _vault[name] = self
    }

    this.getSprite = function (){
        return sprite
    }

    this.getIndex = function () {
        return _index
    }

    this.setIndex = function (value) {
        _index = value
    }

    this.remove = function (){
        Creator.eventsManager.removeEvent(Creator.events.OBJECT_SELECTED, onObjectSelected, this)
        Creator.eventsManager.removeEvent(Creator.events.ALL_OBJECTS_UNSELECTED, onObjectUnselected, this)
        container.removeChild(content)
    }

    Creator.eventsManager.addEvent(Creator.events.OBJECT_SELECTED, onObjectSelected, this)
    Creator.eventsManager.addEvent(Creator.events.OBJECT_UPDATED, (event) => {
        if (event.data === sprite) {
            updateName(sprite.getName())
        }
    }, this)
    Creator.eventsManager.addEvent(Creator.events.ALL_OBJECTS_UNSELECTED, onObjectUnselected, this)
}

module.exports.HierarchyItem = HierarchyItem