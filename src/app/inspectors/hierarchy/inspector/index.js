const Hierarchy = function (container) {
    const {HierarchyItem} = require(path.resolve(__dirname, '../item'));
    const self = this

    let items = {}
    let order = []
    let placeholders = []
    
    const content = document.createElement('div')
    const caption = document.createElement('hierarchy-caption')
    const image = document.createElement('img')    
    const captiontext = document.createTextNode('Hierarchy')
    const vertscrollnode = document.createElement('hierarchy-vert-scrollnode')
    
    vertscrollnode.className = 'hierarchy-vert-scrollnode'
    image.src = 'res/hierarchy.png'
    image.className = 'inspector-caption-image'
    caption.className = 'inspector-caption'
    content.id = "hierarchy"

    if (vertscrollnode.addEventListener) {
        vertscrollnode.addEventListener('mouseup',(event) => {
            Creator.drag.setItem(null)
            document.body.style.cursor = "default";
        })

        vertscrollnode.addEventListener('mousemove',(event) => {
            if (Creator.drag.getItem()) {
                document.body.style.cursor = "alias";
            }  
        })
    }

    const findNextItemName = function (name){
        let index = 0
        while (items.hasOwnProperty(`${name}_${index}`)) {
            index ++
        }
        return `${name}_${index}`
    }

    const reorderItems = function (){
        placeholders.forEach(pl => {
            vertscrollnode.removeChild(pl)
        })
        placeholders = []
        order.forEach((item, index) => {
            appendPlaceholder()
            item.getSprite().setOrder((index + 1) * 100)
            item.setIndex(index)
            item.getContent().style.order = index * 2 + 1
        })
        appendPlaceholder()
    }

    const setItemToIndex = function (item, index){
        Creator.utils.moveInArray(order, item.getIndex(), index)
        reorderItems()
    }

    const appendPlaceholder = function () {
        const pholder = document.createElement('inspector-placeholder')
        pholder.className = 'inspector-placeholder'
        pholder.addEventListener('mousemove', (event)=> {
            if (Creator.drag.getItem()){
                if (Creator.drag.getItem().getIndex() !== pholder.index && Creator.drag.getItem().getIndex() + 1 !== pholder.index){
                    pholder.className = 'inspector-placeholder active'
                }  
            }
        })
        pholder.addEventListener('mouseup', (event)=> {
            if (Creator.drag.getItem()){
                if (Creator.drag.getItem().getIndex() !== pholder.index && Creator.drag.getItem().getIndex() + 1 !== pholder.index){
                    pholder.className = 'inspector-placeholder'
                    setItemToIndex (Creator.drag.getItem(), pholder.index)
                    Creator.drag.setItem(null)
                }
            }
        })
        pholder.addEventListener('mouseleave', (event)=> {
            pholder.className = 'inspector-placeholder'
        })
        pholder.style.order = placeholders.length * 2
        pholder['index'] = placeholders.length
        placeholders.push(pholder)
        vertscrollnode.appendChild(pholder)
    }

    const onEntityAdded = function(event) {
        const sprite = event.data
        const name = items.hasOwnProperty(sprite.getName()) ? findNextItemName(sprite.getName()) : sprite.getName() 
        const item = new HierarchyItem(vertscrollnode, event.data, name, order, items)
        order.push(item)
        items[name] = item
        reorderItems()
    }

    
    const onEntityDeleted = function(event) {
        const name = event.data
        items[name].remove()
        order.splice(order.indexOf(items[name]), 1)
        delete items[name]
        reorderItems()
    }

    caption.appendChild(image)
    caption.appendChild(captiontext)
    content.appendChild(caption)
    content.appendChild(vertscrollnode)

    Creator.eventsManager.addEvent(Creator.events.OBJECT_CREATED, onEntityAdded, this)
    Creator.eventsManager.addEvent(Creator.events.OBJECT_DELETED, onEntityDeleted, this)
    
    container.appendChild(content)
}

module.exports.Hierarchy = Hierarchy