
const NodeTypeBlock = function () {
    const block = document.createElement('position-block')
    const container = document.createElement('captioned-block-container')

    block.className = 'single-property-block'

    const nameLabel = document.createElement('nameLabel')
    nameLabel.className = 'block-node-type'
    nameLabel.innerText = 'Node type'

    block.appendChild(nameLabel);

    let current, previous

    this.update = function(source) {
        current = source;
        previous = source.nodeType
        nameLabel.innerText = previous
    }

    this.getBlockElement = function () {
        return block;
    }

    this.setEnabled = function (value) {
        block.style.display = value ? 'block' : 'none'
    }
}

module.exports.NodeTypeBlock = NodeTypeBlock