const { ActionType, Action } = require("../../actions/action")
const { PropActionRecordGuard } = require("../../actions/guards/propActionRecordGuard")
const { SpriteNameActionGuard } = require("../../actions/guards/spriteNameActionGuard")
const { FoldList } = require("../../ui/FoldList")
const { Label } = require("../../ui/Label")
const { Styles } = require("../../ui/Styles")
const { FoldListItemView } = require("./foldListItemView")

const FoldedListViewBlock = function (config) {
    const block = document.createElement('position-block')
    const views = {}
    let current, previous

    block.className = 'single-property-block'

    const captionLabel = new Label(block, {text : config.caption ? config.caption : 'Unknown property : ', style : Styles.LABEL_CAPTION, hint : config.hint})

    this.updateTargetValue = function (variant){
        if (!current) return;

        this.showView(variant.value);

        Creator.actionManager.makeAction(new Action( 
                current, 
                ActionType.SET_PROPERTY,
                { value : variant.value, prop : config.prop}
            ), 
            new PropActionRecordGuard()
        )
        
    }

    const foldList = new FoldList(block, config.variants, (value) => this.updateTargetValue(value))

    config.variants.forEach(element => {
        if (element.value.view){
            views[element.value.value] = new FoldListItemView(block, element.value.view)
            views[element.value.value].setEnabled(false)
        } else {
            views[element.value.value] = null;
        }
    })

    this.getBlockElement = function () {
        return block;
    }

    this.setEnabled = function (value) {
        block.style.display = value ? 'block' : 'none'  
        if (!value) {
            foldList.fold()
        }
    }

    this.update = function(source) {
        current = source;
        previous = source.getProperty(config.prop)
        foldList.setValue(previous, (a) => a.value.value === previous)
        this.showView(previous)
        config.variants.forEach(element => {
            if (views[element.value.value]){
                views[element.value.value].update(source)
            }
        })
    }
    
    this.showView = function (id) {
        Object.keys(views).forEach(key =>{
            if (views[key]){
                views[key].setEnabled(key === id)
            }
        })
    }
}

module.exports.FoldedListViewBlock = FoldedListViewBlock