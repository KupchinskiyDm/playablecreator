const colorjoe = require("colorjoe");
const { Label } = require("../../ui/Label");
const { Styles } = require("../../ui/Styles");

const PropertyTintField = function () {
    const self = this
    const sliderRatio = 100
    let currentTint = null;

    const block = document.createElement('position-block')
    block.className = 'multy-property-block'

    const nameLabel = new Label(block, {style : Styles.LABEL_CAPTION, text : 'Tint : ', hint : 'Tint'})

    // ---------------------

    const tintRangeContainer = document.createElement('block-container')
    tintRangeContainer.className = 'block-container'

    const tintRangeLabel = document.createElement('nameLabel')
    tintRangeLabel.className = 'block-container-label'
    tintRangeLabel.innerText = 'B : '

    const tintRange = document.createElement('input')
    tintRange.className = 'properties-slider'
    tintRange.type = 'range'
    tintRange.min = 0
    tintRange.max = 100
    tintRange.value = 100

    const tintRangeField = document.createElement('input')
    tintRangeField.className = 'properties-number-field'
    tintRangeField.type = 'text'
    tintRangeField.value = '1'

    tintRangeContainer.appendChild(tintRangeLabel);
    tintRangeContainer.appendChild(tintRange);
    tintRangeContainer.appendChild(tintRangeField);
    block.appendChild(tintRangeContainer);

    tintRangeField.addEventListener('keydown', getInputFieldCallback(tintRangeField, tintRange));
    tintRangeField.addEventListener("focusout", getInputFieldCallback(tintRangeField, tintRange, true));
    tintRangeField.addEventListener("focus", function () {
        tintRangeField.select()
    });

     //------------------------
    
    const tintBrightnessContainer = document.createElement('block-container')
    tintBrightnessContainer.className = 'block-container'

    const tintBrightnessLabel = document.createElement('nameLabel')
    tintBrightnessLabel.className = 'block-container-label'
    tintBrightnessLabel.innerText = 'V : '

    const tintBrightness = document.createElement('input')
    tintBrightness.className = 'properties-slider'
    tintBrightness.type = 'range'
    tintBrightness.min = 0
    tintBrightness.max = 100
    tintBrightness.value = 100
   
    const tintBrightnessField = document.createElement('input')
    tintBrightnessField.className = 'properties-number-field'
    tintBrightnessField.type = 'text'
    tintBrightnessField.value = '1'

    tintBrightnessContainer.appendChild(tintBrightnessLabel);
    tintBrightnessContainer.appendChild(tintBrightness);
    tintBrightnessContainer.appendChild(tintBrightnessField);
    block.appendChild(tintBrightnessContainer);

    tintBrightnessField.addEventListener('keydown', getInputFieldCallback(tintBrightnessField, tintBrightness));
    tintBrightnessField.addEventListener("focusout", getInputFieldCallback(tintBrightnessField, tintBrightness, true));
    tintBrightnessField.addEventListener("focus", function () {
        tintBrightnessField.select()
    });
    // ----------------------------
    
    const tintColorContainer = document.createElement('block-container')
    tintColorContainer.className = 'block-container'

    const tintColorLabel = document.createElement('nameLabel')
    tintColorLabel.className = 'block-container-label'
    tintColorLabel.innerText = 'C : '

    const tintColorField = document.createElement('text')
    tintColorField.className = 'color-picker-field'

    tintColorContainer.appendChild(tintColorLabel);
    tintColorContainer.appendChild(tintColorField);
    block.appendChild(tintColorContainer);
    
    //-----------------------------

    var colorPickerContainer = document.createElement('div')
    colorPickerContainer.className = 'color-picker-container'
    colorPickerContainer.style.display = 'none'
    var colorPickerElement = document.createElement('color-picker-element')
    colorPickerContainer.appendChild(colorPickerElement)
    // document.body.appendChild(colorPickerContainer)

    const picker = colorjoe.rgb(colorPickerElement, '#ffffff', [
        'currentColor',
        ['fields', {space: 'RGB', limit: 255, fix: 0}],
        'hex'
    ]);

    picker.hide();

    //--------------------------------

    function getInputFieldCallback (field, slider, auto = false) {
        return (event) => {
            if (event.keyCode === 13 || auto) {
                let parsed = Number.parseFloat(field.value)
                let newValue = (Number.isNaN(parsed) ? 1 : parsed) * sliderRatio
                newValue = Creator.utils.checkMinMax(0, sliderRatio, newValue) ? newValue : sliderRatio
                if (!Creator.utils.checkMinMax(0, sliderRatio, newValue)){
                    field.value = '1'
                }
                slider.value = (Number.isNaN(parsed) ? 1 : parsed) * sliderRatio
                self.updateTargetValue()
            }
        }
    }

    let justSelected = false;
    let current, x

    this.update = function(source, selected) {
        current = source;
        currentTint = current.getTint()
        if (!currentTint) {
            currentTint = {
                tintValue : 1,
                tintBrightness : 1,
                color: '#ffffff',
                originalColor: '#ffffff',
                rgb: [255, 255, 255]
            }
        }
        tintBrightnessField.value = currentTint.tintBrightness
        tintRangeField.value = currentTint.tintValue
        tintRange.value = currentTint.tintValue * sliderRatio
        tintBrightness.value = currentTint.tintBrightness * sliderRatio
        tintColorField.style.background = currentTint.color
        if (selected) {
            justSelected = true;
            picker.set(currentTint.originalColor)
        }
    }

    this.getBlockElement = function () {
        return block;
    }

    function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }
      
    function rgbToHex(r, g, b) {
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }

    this.updateTargetValue = function (event){
        if (justSelected){
            justSelected = false
            return
        }
        if (!current) return;
        if (!currentTint) {
            currentTint = {}
            tintRange.value = sliderRatio
            tintBrightness.value = sliderRatio
        }
        currentTint.tintValue = Creator.utils.checkMinMax(0, 1, tintRange.value / sliderRatio) ? Number.parseFloat(tintRange.value / sliderRatio) : 1
        currentTint.tintBrightness = Creator.utils.checkMinMax(0, 1, tintBrightness.value / sliderRatio) ? Number.parseFloat(tintBrightness.value / sliderRatio) : 1

        const rgb = picker.get().rgb()

        const rgba = PIXI.utils.premultiplyRgba(
            [
                rgb.r() * 255, 
                rgb.g() * 255, 
                rgb.b() * 255
            ], 
            currentTint.tintValue
        )

        let r = Math.round(rgba[0]);
		let g = Math.round(rgba[1]);
		let b = Math.round(rgba[2]);
		
        const rgbaBright = [
            r +  Math.round((255 - r) * (1 - currentTint.tintBrightness)),
            g +  Math.round((255 - g) * (1 - currentTint.tintBrightness)),
            b +  Math.round((255 - b) * (1 - currentTint.tintBrightness))
        ]

        currentTint.rgb = rgbaBright
        currentTint.color = rgbToHex(rgbaBright[0], rgbaBright[1], rgbaBright[2])
        currentTint.originalColor = rgbToHex(Math.round(rgb.r() * 255), Math.round(rgb.g() * 255), Math.round(rgb.b() * 255))
        current.setTint(currentTint)
    }
    
    this.setEnabled = function (value) {
        block.style.display = value ? 'block' : 'none'
    }

    picker.on('change', pick => {
        this.updateTargetValue();
    }).update();

    colorPickerContainer.addEventListener("click", event => {
        if (event.target === colorPickerContainer){
            picker.hide();
            colorPickerContainer.style.display = 'none'
        }
    }) 

    tintRange.oninput = this.updateTargetValue;
    tintBrightness.oninput = this.updateTargetValue;

    tintColorField.addEventListener("mousedown", (event) => {
        colorPickerContainer.style.display = 'initial'
        picker.show();
    })
}

module.exports.PropertyTintField = PropertyTintField