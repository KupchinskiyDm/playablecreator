const { Action, ActionType } = require("../../actions/action")
const { PropActionRecordGuard } = require("../../actions/guards/propActionRecordGuard")
const { ColorField } = require("../../ui/ColorField")
const { InputField } = require("../../ui/InputField")
const { Label } = require("../../ui/Label")
const { Styles } = require("../../ui/Styles")

const TimelineColorFieldBlock = function (parent, config) {
    let current, x, y

    const container = document.createElement('block-container')
    container.className = 'block-container'

    const xLabel = new Label(container, {text : config.captionX ? config.captionX : 'T : ', hint: config.hintX})
    const xPosField = new InputField(container, {value : config.defX ? config.defX : 0}, () => this.updateTargetValue())
    const yLabel = new Label(container, {text : config.captionY ? config.captionY : 'C : ', hint: config.hintY})
    const colorField = new ColorField(container, {value : config.defY ?  config.defY : 0}, () => this.updateTargetValue())


    parent.appendChild(container)

    this.update = function(source) {
        current = source;

        x = source.getProperty(config.propX)
        xPosField.element.value = Number((x).toFixed(3))

        y = source.getProperty(config.propY)
        colorField.setValue(y)
    }

    this.getBlockElement = function () {
        return container;
    }

    this.setEnabled = function (value) {
        
    }

    this.updateTargetValue = function (event){
        if (!current) return;
        const nx = Creator.utils.checkFloatMatch(xPosField.element.value) ? Number.parseFloat(xPosField.element.value) : x
        const ny = colorField.getValue()

        if (nx !== x) Creator.actionManager.makeAction(new Action(current, ActionType.SET_PROPERTY, { prop : config.propX, value : nx }), new PropActionRecordGuard())
        if (ny !== y) Creator.actionManager.makeAction(new Action(current, ActionType.SET_PROPERTY, { prop : config.propY, value : ny }), new PropActionRecordGuard())
    }
}

module.exports.TimelineColorFieldBlock = TimelineColorFieldBlock