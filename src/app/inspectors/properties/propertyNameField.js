const { ActionType, Action } = require("../../actions/action")
const { SpriteNameActionGuard } = require("../../actions/guards/spriteNameActionGuard")

const PropertyNameField = function () {
    const block = document.createElement('position-block')
    const container = document.createElement('captioned-block-container')

    block.className = 'single-property-block'

    const nameLabel = document.createElement('nameLabel')
    nameLabel.className = 'block-container-caption'
    nameLabel.innerText = 'Name : '

    const nameField = document.createElement('input')
    nameField.className = 'properties-name-field'
    nameField.type = 'text'
    nameField.value = 'name'


    block.appendChild(nameLabel);
    block.appendChild(nameField);

    let current, previous

    this.update = function(source) {
        current = source;
        previous = source.getName()
        nameField.value = previous
    }

    this.getBlockElement = function () {
        return block;
    }

    this.setEnabled = function (value) {
        block.style.display = value ? 'block' : 'none'
    }

    this.updateTargetValue = function (event){
        if (!current) return;
        Creator.actionManager.makeAction(new Action( 
                current, 
                ActionType.SET_NAME,
                { value :  nameField.value.split('').length > 0 ? nameField.value : previous}
            ), 
            new SpriteNameActionGuard()
        )
    }

    nameField.addEventListener("focusout", this.updateTargetValue);
    nameField.addEventListener("focus", function () {
        nameField.select()
    });

    nameField.addEventListener('keydown', (event) => {
        if (event.keyCode === 13) {
            this.updateTargetValue()
        }
    });
}

module.exports.PropertyNameField = PropertyNameField