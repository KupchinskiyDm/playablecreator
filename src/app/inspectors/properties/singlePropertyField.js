const { ActionType, Action } = require("../../actions/action")
const { PropActionRecordGuard } = require("../../actions/guards/propActionRecordGuard")
const { InputField } = require("../../ui/InputField")
const { Label } = require("../../ui/Label")
const { Styles } = require("../../ui/Styles")

const SinglePropertyField = function (config = {}) {
    let current, x
    const block = document.createElement('position-block')
    block.className = 'single-property-block'

    const captionLabel = new Label(block, {text : config.caption ? config.caption : 'Unknown property : ', style : Styles.LABEL_CAPTION, hint : config.hint})
    const inputField = new InputField(block, {value : config.default ?  config.default : 0}, () => this.updateTargetValue())

    this.update = function(source) {
        current = source;
        x = source.getProperty(config.prop)
        if (x === NaN){
            inputField.element.value = 'NaN';
        } else {
            inputField.element.value = Number((x).toFixed(4))
        }
    }

    this.getBlockElement = function () {
        return block;
    }

    this.updateTargetValue = function (event){
        if (!current) return;
        const nx = Creator.utils.checkFloatMatch(inputField.element.value) ? Number.parseFloat(inputField.element.value) : x
        if (nx !== x) Creator.actionManager.makeAction(new Action(current, ActionType.SET_PROPERTY, { prop : config.prop, value : nx }), new PropActionRecordGuard())
    }

    this.setEnabled = function (value) {
        block.style.display = value ? 'block' : 'none'
    }
}

module.exports.SinglePropertyField = SinglePropertyField