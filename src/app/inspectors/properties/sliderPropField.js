const { ActionType, Action } = require("../../actions/action")
const { PropActionRecordGuard } = require("../../actions/guards/propActionRecordGuard")
const { InputField } = require("../../ui/InputField")
const { Label } = require("../../ui/Label")
const { Slider } = require("../../ui/Slider")
const { Styles } = require("../../ui/Styles")

const SliderPropField = function (config) {
    const sliderRatio = 100
    const self = this
    let current, x
    
    const block = document.createElement('position-block')
    block.className = 'single-property-block'

    const nameLabel = new Label(block, {style : Styles.LABEL_CAPTION, text :  config.caption ? config.caption : 'Amount : ', hint : 'Amount'})
    const alphaRange = new Slider(block, {style : Styles.SLIDER_SHIFTED_TOP}, (...args) => this.updateTargetValue(...args))
    const alphaField = new InputField(block, {}, (...args) => this.parseAndUpdateValue(...args), alphaRange)

    this.update = function(source) {
        current = source;
        x = source.getProperty(config.prop)
        alphaRange.element.value = x * sliderRatio
        alphaField.element.value = Number((x).toFixed(3))
    }

    this.getBlockElement = function () {
        return block;
    }

    this.updateTargetValue = function (emitter, connected, discard, force, lastValue = -1){
        if (!current) return;
        const value = Creator.utils.checkMinMax(0, 1, alphaRange.element.value / sliderRatio) ? Number.parseFloat(alphaRange.element.value / sliderRatio) : x
        let last = null
        if (lastValue > -1) {
            last = Creator.utils.checkMinMax(0, 1, lastValue / sliderRatio) ? Number.parseFloat(lastValue / sliderRatio) : x
        }
        Creator.actionManager.makeAction(new Action(current, ActionType.SET_PROPERTY, { prop : config.prop, value }, {prop : config.prop, value :last}), new PropActionRecordGuard(discard, force))
    }
    
    this.setEnabled = function (value) {
        block.style.display = value ? 'block' : 'none'
    }

    this.parseAndUpdateValue = function (field, slider) {
        let parsed = Number.parseFloat(field.element.value)
        if (slider) {
            slider.element.value = (Number.isNaN(parsed) ? x : parsed) * sliderRatio
        }
        self.updateTargetValue()
    }
}

module.exports.SliderPropField = SliderPropField