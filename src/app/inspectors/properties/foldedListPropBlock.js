const { ActionType, Action } = require("../../actions/action")
const { PropActionRecordGuard } = require("../../actions/guards/propActionRecordGuard")
const { SpriteNameActionGuard } = require("../../actions/guards/spriteNameActionGuard")
const { FoldList } = require("../../ui/FoldList")
const { Label } = require("../../ui/Label")
const { Styles } = require("../../ui/Styles")

const FoldedListPropertyBlock = function (config) {
    const block = document.createElement('position-block')
    const container = document.createElement('captioned-block-container')
    let current, previous

    block.className = 'single-property-block'

    const captionLabel = new Label(block, {text : config.caption ? config.caption : 'Unknown property : ', style : Styles.LABEL_CAPTION, hint : config.hint})

    this.updateTargetValue = function (value){
        if (!current) return;
        Creator.actionManager.makeAction(new Action( 
                current, 
                ActionType.SET_PROPERTY,
                { value, prop : config.prop}
            ), 
            new PropActionRecordGuard()
        )
    }

    const foldList = new FoldList(block, config.variants, (value) => this.updateTargetValue(value))
    
    this.getBlockElement = function () {
        return block;
    }

    this.setEnabled = function (value) {
        block.style.display = value ? 'block' : 'none'  
        if (!value) {
            foldList.fold()
        }
    }

    this.update = function(source) {
        current = source;
        previous = source.getProperty(config.prop)
        foldList.setValue(previous)
    }
}

module.exports.FoldedListPropertyBlock = FoldedListPropertyBlock