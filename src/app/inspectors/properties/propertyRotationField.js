const { ActionType, Action } = require("../../actions/action")
const { PropActionRecordGuard } = require("../../actions/guards/propActionRecordGuard")

const PropertyRotationField = function (_caption = null) {
    const block = document.createElement('position-block')
    block.className = 'single-property-block'

    const nameLabel = document.createElement('nameLabel')
    nameLabel.className = 'block-container-caption'
    nameLabel.innerText = _caption ? _caption : 'Rotation : '

    const rotationField = document.createElement('input')
    rotationField.className = 'properties-number-field'
    rotationField.type = 'text'
    rotationField.value = '0'


    block.appendChild(nameLabel);
    block.appendChild(rotationField);

    let current, x

    this.update = function(source) {
        current = source;
        x = source.getContent().rotation
        rotationField.value = Number((x).toFixed(4))
    }

    this.getBlockElement = function () {
        return block;
    }

    this.updateTargetValue = function (event){
        if (!current) return;
        const value = Creator.utils.checkFloatMatch(rotationField.value) ? Number.parseFloat(rotationField.value) : x
        Creator.actionManager.makeAction(new Action(current, ActionType.SET_PROPERTY, { prop : 'rotation', value }), new PropActionRecordGuard())
    }

    this.setEnabled = function (value) {
        block.style.display = value ? 'block' : 'none'
    }

    rotationField.addEventListener("focusout", this.updateTargetValue);
    rotationField.addEventListener("focus", function () {
        rotationField.select()
    });
    rotationField.addEventListener('keydown', (event) => {
        if (event.keyCode === 13) {
            this.updateTargetValue()
        }
    });
}

module.exports.PropertyRotationField = PropertyRotationField