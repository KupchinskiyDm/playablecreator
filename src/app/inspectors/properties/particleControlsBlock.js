

const { Action, ActionType } = require("../../actions/action")
const { PropActionRecordGuard } = require("../../actions/guards/propActionRecordGuard")
const { Button } = require("../../ui/Button")
const { InputField } = require("../../ui/InputField")
const { Label } = require("../../ui/Label")
const { Styles } = require("../../ui/Styles")

const ParticleControlsBlock = function (config) {
    let current

    const block = document.createElement('multy-property-block')
    const container = document.createElement('block-container')

    block.className = 'multy-property-block'
    container.className = 'block-container'

    block.appendChild(container)

    if (config.buttons.indexOf('export') !== -1){
        const exportButton = new Button(container, {text: 'Export', hint: 'Export to file'}, () => {
            current.save()
        })
    }

    if (config.buttons.indexOf('import') !== -1){
        const importButton = new Button(container, {text: 'Import', hint: 'Import from file'}, () => {
            current.import()
        })
    }
   
    if (config.buttons.indexOf('reset') !== -1){
        const resetButton = new Button(container, {text: 'Reset', hint: 'Reset to default'}, () => {
            Creator.dialog.showMessageBox(Creator.electronApp.getCurrentWindow(),{
                message : 'Please confirm resetting particles system to default state.', 
                type : 'question',
                buttons : ['Reset', 'Cancel'],
                cancelId : 1
            }).then(result => {
                if (result.response === 0){
                    current.reset()
                }
            })
        })
    }

    if (config.buttons.indexOf('play') !== -1){
        const playButton = new Button(container, {text: 'Play', hint: 'Emit true'}, () => {
            current.play()
        })
    }

    if (config.buttons.indexOf('stop') !== -1){
        const stopButton = new Button(container, {text: 'Stop', hint: 'Emit false'}, () => {
            current.stop()
        })
    }

    this.update = function(source) {
        current = source;
    }

    this.getBlockElement = function () {
        return block;
    }

    this.setEnabled = function (value) {
        block.style.display = value ? 'block' : 'none'
    }
}

module.exports.ParticleControlsBlock = ParticleControlsBlock