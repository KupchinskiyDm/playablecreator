const { Action, ActionType } = require("../../actions/action")
const { PropActionRecordGuard } = require("../../actions/guards/propActionRecordGuard")
const { InputField } = require("../../ui/InputField")
const { Label } = require("../../ui/Label")
const { Styles } = require("../../ui/Styles")

const XYPropertyBlock = function (config) {
    let current, x, y

    const block = document.createElement('multy-property-block')
    const container = document.createElement('block-container')

    block.className = 'multy-property-block'
    container.className = 'block-container'

    const blockLabel = new Label(block, {style : Styles.LABEL_CAPTION, text : config.label + ' : ', hint : config.label})
    const xLabel = new Label(container, {text : config.captionX ? config.captionX : 'X : ', hint : 'X'})
    const xPosField = new InputField(container, {value : config.defX}, () => this.updateTargetValue())
    const yLabel = new Label(container, {text : config.captionY ? config.captionY : 'Y : ', hint : 'Y'})
    const yPosField = new InputField(container, {value : config.defY}, () => this.updateTargetValue())

    block.appendChild(container)

    this.update = function(source) {
        current = source;
        x = source.getContent()[config.prop].x
        y = source.getContent()[config.prop].y
        xPosField.element.value = Number((x).toFixed(3))
        yPosField.element.value = Number((y).toFixed(3))
    }

    this.getBlockElement = function () {
        return block;
    }

    this.setEnabled = function (value) {
        block.style.display = value ? 'block' : 'none'
    }

    this.updateTargetValue = function (event){
        if (!current) return;
        let value = {
            x : Creator.utils.checkFloatMatch(xPosField.element.value) ? Number.parseFloat(xPosField.element.value) : x,
            y : Creator.utils.checkFloatMatch(yPosField.element.value) ? Number.parseFloat(yPosField.element.value) : y
        }
        Creator.actionManager.makeAction(new Action(current, ActionType.SET_PROPERTY, { prop : config.prop, value }), new PropActionRecordGuard())

    }
}

module.exports.XYPropertyBlock = XYPropertyBlock