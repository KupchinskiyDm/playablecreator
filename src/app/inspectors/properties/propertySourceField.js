const PropertySourceField = function () {
    const self = this
    const block = document.createElement('position-block')

    block.className = 'single-property-block'

    const nameLabel = document.createElement('nameLabel')
    nameLabel.className = 'block-container-caption'
    nameLabel.innerText = 'Src : \206 '

    const pathField = document.createElement('input')
    pathField.className = 'properties-path-field'
    pathField.type = 'text'
    pathField.value = 'name'
    pathField.wrapper = this

    Creator.objectResPathSetting = this;

    block.appendChild(nameLabel);
    block.appendChild(pathField);

    let current, chachedRespath

    this.update = function(source) {
        current = source;
        chachedRespath = source.getResPath()
        pathField.value = source.getResPathFormatted()
    }

    this.setHighlight = function (value) {
        pathField.className = value ? 'properties-path-field highlight' : 'properties-path-field'
    }

    this.getBlockElement = function () {
        return block;
    }

    this.setEnabled = function (value) {
        block.style.display = value ? 'block' : 'none'
    }

    this.updateTargetValue = function (event){
        if (!current) return;
        current.setResPath(chachedRespath)
    }

    const onDropAsset = function (e) {
        if(!e || !e.data || !e.data.event) return
        var x = e.data.event.clientX;
        var y = e.data.event.clientY;
        target = document.elementFromPoint(e.data.event.clientX, e.data.event.clientY);
        if (target === pathField){
            if (!current) return;
            const extension = e.data.item.resname.split('.')[1];
            if (current.getAllowedResExtensions().find(element => element === extension)){
                chachedRespath = e.data.item.respath
                self.updateTargetValue()
            }
        }
    }

    pathField.addEventListener("focusout", this.updateTargetValue);

    pathField.addEventListener('keydown', (event) => {
        if (event.keyCode === 13) {
            this.updateTargetValue()
        }
    });
    

    Creator.eventsManager.addEvent(Creator.events.DROP_ASSET, onDropAsset, this)
}

module.exports.PropertySourceField = PropertySourceField