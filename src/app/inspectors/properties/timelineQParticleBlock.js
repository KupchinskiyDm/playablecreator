const { Action, ActionType } = require("../../actions/action")
const { PropActionRecordGuard } = require("../../actions/guards/propActionRecordGuard")
const { Button } = require("../../ui/Button")
const { InputField } = require("../../ui/InputField")
const { Label } = require("../../ui/Label")
const { Styles } = require("../../ui/Styles")
const { SimpleDoubleFieldBlock } = require("./simpleDoubleFieldBlock")
const { TimelineColorFieldBlock } = require("./timelineColorFieldBlock")

const TimelineQParticleBlock = function (config) {
    let current, x, y

    const self = this
    const block = document.createElement('multy-property-block')
    const container = document.createElement('block-container')

    block.className = 'multy-property-block'
    container.className = 'flex-column-container'
    const captionBlock = document.createElement('multy-property-caption-block')
    captionBlock.style.display = 'flex'
    captionBlock.style.flexDirection = 'row'
    // TODO: ADD FOLD

    const foldButton = new Button(captionBlock, {image: 'res/button_up.png'}, () => {
        self.setFolded(true)
    })

    const unfoldButton = new Button(captionBlock, {image: 'res/button_down.png'}, () => {
        self.setFolded(false)
    })

    const blockLabel = new Label(captionBlock, {style : Styles.LABEL_CAPTION, text : config.caption, hint : config.hint})

    block.appendChild(captionBlock)

    const timeline = []
    
    config.timeline.forEach(frame => {
        switch (frame.valueType) {
            case 'float':
                timeline.push(new SimpleDoubleFieldBlock(container, frame))
                break;
            case 'color':
                timeline.push(new TimelineColorFieldBlock(container, frame))
                break;

            default:
                break;
        }

        
    })

    block.appendChild(container)

    this.setFolded = function (value) {
        container.style.display = value ? 'none' : 'flex'
        foldButton.element.style.display =  value ? 'none' : 'block'
        unfoldButton.element.style.display =  !value ? 'none' : 'block'
    }

    this.update = function(source) {
        current = source;
        timeline.forEach(frame => frame.update(source));
    }

    this.getBlockElement = function () {
        return block;
    }

    this.setEnabled = function (value) {
        block.style.display = value ? 'block' : 'none'
    }

    self.setFolded(true)
}

module.exports.TimelineQParticleBlock = TimelineQParticleBlock