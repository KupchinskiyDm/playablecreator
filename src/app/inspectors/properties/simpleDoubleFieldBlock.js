const { Action, ActionType } = require("../../actions/action")
const { PropActionRecordGuard } = require("../../actions/guards/propActionRecordGuard")
const { InputField } = require("../../ui/InputField")
const { Label } = require("../../ui/Label")
const { Styles } = require("../../ui/Styles")

const SimpleDoubleFieldBlock = function (parent, config) {
    let current, x, y

    const container = document.createElement('block-container')
    container.className = 'block-container'

    const xLabel = new Label(container, {text : config.captionX ? config.captionX : 'X : ', hint: config.hintX})
    const xPosField = new InputField(container, {value : config.defX ? config.defX : 0}, () => this.updateTargetValue())
    const yLabel = new Label(container, {text : config.captionY ? config.captionY : 'Y : ', hint: config.hintY})
    const yPosField = new InputField(container, {value : config.defY ?  config.defY : 0}, () => this.updateTargetValue())

    if (!config.propX){
        xLabel.element.style.display = 'none'
        xPosField.element.style.display = 'none'
    }

    if (!config.propY){
        yLabel.element.style.display = 'none'
        yPosField.element.style.display = 'none'
    }


    parent.appendChild(container)

    this.update = function(source) {
        current = source;
        if (config.propX){
            x = source.getProperty(config.propX)
            xPosField.element.value = Number((x).toFixed(3))
        }
        if (config.propY){
            y = source.getProperty(config.propY)
            yPosField.element.value = Number((y).toFixed(3))
        }
    }

    this.getBlockElement = function () {
        return container;
    }

    this.setEnabled = function (value) {
        
    }

    this.updateTargetValue = function (event){
        if (!current) return;
        const nx = Creator.utils.checkFloatMatch(xPosField.element.value) ? Number.parseFloat(xPosField.element.value) : x
        const ny = Creator.utils.checkFloatMatch(yPosField.element.value) ? Number.parseFloat(yPosField.element.value) : y

        if (config.propX && nx !== x) Creator.actionManager.makeAction(new Action(current, ActionType.SET_PROPERTY, { prop : config.propX, value : nx }), new PropActionRecordGuard())
        if (config.propY && ny !== y) Creator.actionManager.makeAction(new Action(current, ActionType.SET_PROPERTY, { prop : config.propY, value : ny }), new PropActionRecordGuard())
    }
}

module.exports.SimpleDoubleFieldBlock = SimpleDoubleFieldBlock