const { AppModel } = require("../../models");
const { FoldedListPropertyBlock } = require("./foldedListPropBlock");
const { FoldedListViewBlock } = require("./foldedListWithViewBlock");
const { NodeTypeBlock } = require("./nodeTypeBlock");
const { ParticleControlsBlock } = require("./particleControlsBlock");
const { PropertyAlphaField } = require("./propertyAlphaField");
const { PropertyNameField } = require("./propertyNameField");
const { PropertyRotationField } = require("./propertyRotationField");
const { PropertySourceField } = require("./propertySourceField");
const { PropertyTintField } = require("./propertyTintField");
const { SinglePropertyField } = require("./singlePropertyField");
const { SliderPropField } = require("./sliderPropField");
const { TimelineQParticleBlock } = require("./timelineQParticleBlock");
const { XYPropertyBlock } = require("./xyPropertyBlock");

const Properties = function (container) {
    
    const self = this
    let current = null
    const blocks = [];
    const content = document.createElement('div')
    const caption = document.createElement('properties-caption')
    const image = document.createElement('img')
    const captiontext = document.createTextNode('Properties')
    const vertscrollnode = document.createElement('properties-vert-scrollnode')

    vertscrollnode.className = 'inspector-vert-scrollnode'
    image.src = 'res/properties.png'
    image.className = 'inspector-caption-image'
    caption.className = 'inspector-caption'
    content.id = "properties"

    const nodeTypeBlock = new NodeTypeBlock()
    blocks.push(nodeTypeBlock)

    const particleControlsBlock = new ParticleControlsBlock({buttons:['import', 'export', 'reset']});
    blocks.push(particleControlsBlock)

    const nameBlock = new PropertyNameField()
    blocks.push(nameBlock)
    
    const sourceBlock = new PropertySourceField()
    blocks.push(sourceBlock)
    
    const posBlock = new XYPropertyBlock({defX : 0, defY : 0, label : 'Position', prop : 'position'})
    blocks.push(posBlock)

    const scaleBlock = new XYPropertyBlock({defX : 1, defY : 1, label : 'Scale', prop : 'scale'})
    blocks.push(scaleBlock)

    const anchorBlock = new XYPropertyBlock({defX : 0.5, defY : 0.5, label : 'Anchor', prop : 'anchor'})
    blocks.push(anchorBlock)

    const skewBlock = new XYPropertyBlock({defX : 0, defY : 0, label : 'Skew', prop : 'skew'})
    blocks.push(skewBlock)

    const rotationBlock = new PropertyRotationField()
    blocks.push(rotationBlock)

    const alphaBlock = new PropertyAlphaField()
    blocks.push(alphaBlock)

    const blendModesBlock = new FoldedListPropertyBlock({
        caption : 'Blend mode :',
        prop : 'blendMode',
        variants : [
            {
                name : 'NORMAL',
                value : PIXI.BLEND_MODES.NORMAL
            },
            {
                name : 'ADD',
                value : PIXI.BLEND_MODES.ADD
            },
            {
                name : 'SCREEN',
                value : PIXI.BLEND_MODES.SCREEN
            },
            {
                name : 'MULTIPLY',
                value : PIXI.BLEND_MODES.MULTIPLY
            },
            {
                name : 'SRC_ATOP',
                value : PIXI.BLEND_MODES.SRC_ATOP
            },
            {
                name : 'SRC_IN',
                value : PIXI.BLEND_MODES.SRC_IN
            },
            {
                name : 'SRC_OUT',
                value : PIXI.BLEND_MODES.SRC_OUT
            },
            {
                name : 'DST_ATOP',
                value : PIXI.BLEND_MODES.DST_ATOP
            },
            {
                name : 'DST_IN',
                value : PIXI.BLEND_MODES.DST_IN
            },
            {
                name : 'DST_OUT',
                value : PIXI.BLEND_MODES.DST_OUT
            },
            {
                name : 'DST_OVER',
                value : PIXI.BLEND_MODES.DST_OVER
            },
            {
                name : 'NONE',
                value : PIXI.BLEND_MODES.NONE
            }
        ]
    })
    blocks.push(blendModesBlock)

    const tintBlock = new PropertyTintField()
    blocks.push(tintBlock)
    
    
    // particles emitter
    const particlesAlphaTimeline = []
    const particlesScaleTimeline = []
    const particlesSpeedTimeline = []
    const particlesColorTimeline = []
    for (let i = 0; i < 4; i++) {
        particlesAlphaTimeline.push( AppModel.getParticleTimelineConfig('alpha', i))
        particlesScaleTimeline.push( AppModel.getParticleTimelineConfig('scale', i))
        particlesSpeedTimeline.push( AppModel.getParticleTimelineConfig('speed', i))
        particlesColorTimeline.push( AppModel.getParticleTimelineConfig('color', i, 'color'))
    }

    const particleAlphaBlock = new TimelineQParticleBlock({caption : 'Alpha timeline :', timeline : particlesAlphaTimeline})
    blocks.push(particleAlphaBlock)

    const particleScaleBlock = new TimelineQParticleBlock({caption : 'Scale timeline:', timeline : particlesScaleTimeline})
    blocks.push(particleScaleBlock)

    const particleSpeedBlock = new TimelineQParticleBlock({caption : 'Speed timeline :', timeline : particlesSpeedTimeline})
    blocks.push(particleSpeedBlock)

    const particleColorBlock = new TimelineQParticleBlock({caption : 'Color timeline :', timeline : particlesColorTimeline})
    blocks.push(particleColorBlock)

    const startRotationBlock = new TimelineQParticleBlock({caption : 'Start rotation :', timeline : [AppModel.getParticleMinMaxConfig('startRotation')]})
    blocks.push(startRotationBlock)

    const rotationSpeedBlock = new TimelineQParticleBlock({caption : 'Rotation speed :', timeline : [AppModel.getParticleMinMaxConfig('rotationSpeed')]})
    blocks.push(rotationSpeedBlock)

    const lifetimeBlock = new TimelineQParticleBlock({caption : 'Lifetime :', timeline : [AppModel.getParticleMinMaxConfig('lifetime')]})
    blocks.push(lifetimeBlock)

    const accelerationBlock = new TimelineQParticleBlock({caption : 'Acceleration :', timeline : [AppModel.getParticleXYConfig('acceleration')]})
    blocks.push(accelerationBlock)

    const maxSpeedBlock = new SinglePropertyField(AppModel.getParticlePropConfig('maxSpeed'))
    blocks.push(maxSpeedBlock)

    const minimumSpeedMultiplierBlock = new SinglePropertyField(AppModel.getParticlePropConfig('minimumSpeedMultiplier'))
    blocks.push(minimumSpeedMultiplierBlock)

    const minimumScaleMultiplierBlock = new SinglePropertyField(AppModel.getParticlePropConfig('minimumScaleMultiplier'))
    blocks.push(minimumScaleMultiplierBlock)

    const maxParticlesBlock = new SinglePropertyField(AppModel.getParticlePropConfig('maxParticles'))
    blocks.push(maxParticlesBlock)

    const frequencyBlock = new SinglePropertyField(AppModel.getParticlePropConfig('frequency', 'Spawn frequency'))
    blocks.push(frequencyBlock)

    const emitterLifetimeBlock = new SinglePropertyField(AppModel.getParticlePropConfig('emitterLifetime'))
    blocks.push(emitterLifetimeBlock)

    const angleStartMultiplierBlock = new SinglePropertyField(AppModel.getParticlePropConfig('angleStart'))
    blocks.push(angleStartMultiplierBlock)

    const perWaweBlock = new SinglePropertyField(AppModel.getParticlePropConfig('particlesPerWave'))
    blocks.push(perWaweBlock)

    const spacingBlock = new SinglePropertyField(AppModel.getParticlePropConfig('particleSpacing'))
    blocks.push(spacingBlock)

    const spawnPointBlock = new TimelineQParticleBlock({caption : 'Spawn Position :', timeline : [AppModel.getParticleXYConfig('pos')]})
    blocks.push(spawnPointBlock)

    const spawnTypesBlock = new FoldedListViewBlock({
        caption : 'Spawn type :',
        prop : 'emitterData.spawnType',
        variants : [
            {
                name : 'Point',
                value : {
                    value: 'point',
                    view : null
                }
            },
            {
                name : 'Circle',
                value : {
                    value : 'circle',
                    view : [
                        {
                            captionX : 'X :',
                            captionY : 'Y :',
                            hintX : 'X',
                            hintY : 'Y',
                            propX : `emitterData.spawnCircle.x`,
                            propY : `emitterData.spawnCircle.y`,
                            valueType: 'float'
                        },
                        {
                            captionX : 'R :',
                            captionY : 'minR :',
                            hintX : 'Radius',
                            hintY : 'Minimal radius',
                            propX : `emitterData.spawnCircle.r`,
                            propY : `emitterData.spawnCircle.minR`,
                            valueType: 'float'
                        }
                    ]
                }
            },
            {
                name : 'Ring',
                value : {
                    value : 'ring',
                    view : [
                        {
                            captionX : 'X :',
                            captionY : 'Y :',
                            hintX : 'X',
                            hintY : 'Y',
                            propX : `emitterData.spawnCircle.x`,
                            propY : `emitterData.spawnCircle.y`,
                            valueType: 'float'
                        },
                        {
                            captionX : 'R :',
                            captionY : 'minR :',
                            hintX : 'Radius',
                            hintY : 'Minimal radius',
                            propX : `emitterData.spawnCircle.r`,
                            propY : `emitterData.spawnCircle.minR`,
                            valueType: 'float'
                        }
                    ]
                }
            },
            {
                name : 'Rectangle',
                value : {
                    value : 'rect',
                    view : [
                        {
                            captionX : 'X :',
                            captionY : 'Y :',
                            hintX : 'X',
                            hintY : 'Y',
                            propX : `emitterData.spawnRect.x`,
                            propY : `emitterData.spawnRect.y`,
                            valueType: 'float'
                        },
                        {
                            captionX : 'H :',
                            captionY : 'W :',
                            hintX : 'Height',
                            hintY : 'Width',
                            propX : `emitterData.spawnRect.h`,
                            propY : `emitterData.spawnRect.w`,
                            valueType: 'float'
                        }
                    ]
                }
            },
            {
                name : 'Burst',
                value : {
                    value : 'burst',
                    view : null/*[
                        {
                            captionX : 'PW :',
                            captionY : 'S :',
                            hintX : 'Particles per wave',
                            hintY : 'Particle spacing',
                            propX : `emitterData.particlesPerWave`,
                            propY : `emitterData.particleSpacing`,
                            valueType: 'float'
                        },
                        {
                            captionX : 'Angle :',
                            captionY : 'W :',
                            hintX : 'Angle start',
                            hintY : 'Width',
                            propX : `emitterData.angleStart`,
                            propY : null,
                            valueType: 'float'
                        }
                    ]*/
                }
            }
        ]
    })
    blocks.push(spawnTypesBlock)

    const particlePlayPauseBlock = new ParticleControlsBlock({buttons:['play', 'stop']});
    blocks.push(particlePlayPauseBlock)
   

    const types = {
        ParticlesEmitter : [
            nodeTypeBlock, 
            nameBlock, 
            sourceBlock, 
            posBlock, 
            particleAlphaBlock,
            particleScaleBlock,
            particleSpeedBlock,
            startRotationBlock,
            rotationSpeedBlock,
            lifetimeBlock,
            blendModesBlock,
            maxSpeedBlock,
            maxParticlesBlock,
            frequencyBlock,
            minimumSpeedMultiplierBlock,
            minimumScaleMultiplierBlock,
            emitterLifetimeBlock,
            accelerationBlock,
            // spawnPointBlock,
            spawnTypesBlock,
            particleControlsBlock,
            particlePlayPauseBlock,
            angleStartMultiplierBlock,
            perWaweBlock,
            spacingBlock,
            particleColorBlock
        ],
        Sprite : [nodeTypeBlock, nameBlock, sourceBlock, posBlock, scaleBlock, anchorBlock, skewBlock, rotationBlock, alphaBlock, blendModesBlock, tintBlock]
    }
    
    blocks.forEach(block => {
        vertscrollnode.appendChild(block.getBlockElement())
        block.setEnabled(false)
    })

    const onObjectUnselected = function () {
        blocks.forEach(block => {
            block.setEnabled(false)
        })
    }

    const onObjectUpdated = function (event){
        current = event.data
        types[current.nodeType].forEach(block => {
            block.setEnabled(true)
        })
        types[current.nodeType].forEach(block => block.update(current))
    }

    const onObjectSelected = function (event){
        current = event.data
        onObjectUnselected();
        types[current.nodeType].forEach(block => {
            block.setEnabled(true)
        })
        types[current.nodeType].forEach(block => block.update(current, true))
    }

    const onObjectDeleted = function () {
        current = null
        onObjectUnselected();
    }

    this.getCurrent = function () {
        return current;
    }

    // appends (order is matter!)
    caption.appendChild(image)
    caption.appendChild(captiontext)
    content.appendChild(caption)
    content.appendChild(vertscrollnode)
    container.appendChild(content)

    Creator.eventsManager.addEvent(Creator.events.OBJECT_UPDATED, onObjectUpdated, this)
    Creator.eventsManager.addEvent(Creator.events.OBJECT_SELECTED, onObjectSelected, this)
    Creator.eventsManager.addEvent(Creator.events.ALL_OBJECTS_UNSELECTED, onObjectUnselected, this)
    Creator.eventsManager.addEvent(Creator.events.OBJECT_DELETED, onObjectDeleted, this)
}

module.exports.Properties = Properties