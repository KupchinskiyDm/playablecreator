const { Action, ActionType } = require("../../actions/action")
const { PropActionRecordGuard } = require("../../actions/guards/propActionRecordGuard")
const { Button } = require("../../ui/Button")
const { InputField } = require("../../ui/InputField")
const { Label } = require("../../ui/Label")
const { Styles } = require("../../ui/Styles")
const { SimpleDoubleFieldBlock } = require("./simpleDoubleFieldBlock")

const FoldListItemView = function (parent, config){
    let current, x, y

    const self = this
    
    const block = document.createElement('multy-property-block')
    block.className = 'multy-property-block'
    
    const container = block
    // const container = document.createElement('block-container')
    // container.className = 'flex-column-container'
    // block.appendChild(container)
    
    parent.appendChild(block)
    
    const timeline = []
    
    config.forEach(frame => {
        switch (frame.valueType) {
            case 'float':
                timeline.push(new SimpleDoubleFieldBlock(container, frame))
                break;
        
            default:
                break;
        }
    })

    this.update = function(source) {
        current = source;
        timeline.forEach(frame => frame.update(source));
    }

    this.getBlockElement = function () {
        return block;
    }

    this.setEnabled = function (value) {
        block.style.display = value ? 'block' : 'none'
    }
}
module.exports.FoldListItemView = FoldListItemView