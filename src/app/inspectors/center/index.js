const CenterBlock = function (container) {
    const { Canvas } = require(path.resolve(__dirname, '../../canvas/pixi'))
    const { Assets } = require(path.resolve(__dirname, '../assets/inspector'))
    const self = this
    const content = document.createElement('div')
    
    this.canvas = new Canvas(content)
    this.assets = new Assets(content)
    
    content.id = "center"
    container.appendChild(content)
}

module.exports.CenterBlock = CenterBlock