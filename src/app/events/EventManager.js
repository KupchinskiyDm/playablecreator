
const EventManager = (function () {
    let listeners = {};
    this.addEvent = function (eventID, callback, subscriber) {
        if (!listeners[eventID]) {
            listeners[eventID] = [];
        }
        let eventObject = {
            id : eventID,
            cb : callback.bind(subscriber), 
            sb : subscriber
        }
        if (listeners[eventID].length > 0) {
            listeners[eventID].forEach(element => {
                if (element.sb === subscriber && element.cb === callback){
                    return;
                }
            });
        }
        listeners[eventID].push(eventObject);
    };
    this.removeEvent = function (eventID, callback, subscriber) {
        if (listeners[eventID] && listeners[eventID].length > 0) {
            for (let i = 0; i < listeners[eventID].length; i++)
            {
                if  (listeners[eventID][i].cb === callback && listeners[eventID][i].sb === subscriber){
                    listeners[eventID].splice(0, 1);
                    break;
                }
            }
        }
    };
    this.dispatchEvent = function(eventID, params, dispatcher) {
        if (listeners[eventID] && listeners[eventID].length > 0) {
            listeners[eventID].forEach(element => {
                let _params = {
                    event : eventID,
                    data : params, 
                    disp : dispatcher
                };
                element.cb(_params);
            });
            
        }
    }
    return this;
})();

exports.EventManager = EventManager;