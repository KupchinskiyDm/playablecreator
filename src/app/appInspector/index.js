
const AppInspector = function () {
    const  { Hierarchy } = require(path.resolve(__dirname, '../inspectors/hierarchy/inspector'))
    const  { CenterBlock } = require(path.resolve(__dirname, '../inspectors/center'))
    const  { Properties } = require(path.resolve(__dirname, '../inspectors/properties'))

    const container = document.createElement('div')

    this.hierarchyBlock = new Hierarchy(container)
    this.centerBlock = new CenterBlock(container)
    this.propertiesBlock = new Properties(container)

    container.id = "inspector"
    document.body.appendChild(container)
}

module.exports.AppInspector = AppInspector