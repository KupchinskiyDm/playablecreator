const { Styles } = require("./Styles")
const colorjoe = require("colorjoe");

const ColorPicker = function (parent, config) {
    const self = this
    let pickerCallback =null
    this.parent = parent
    this.config = config

    this.element = document.createElement('div')
    this.element.className = 'color-picker-container'
    this.element.style.display = 'none'
    var colorPickerElement = document.createElement('color-picker-element')
    
    const picker = colorjoe.rgb(colorPickerElement, '#ffffff', [
        'currentColor',
        ['fields', {space: 'RGB', limit: 255, fix: 0}],
        'hex'
    ]);

    this.element.addEventListener("click", event => {
        if (event.target === self.element){
            self.hide()
        }
    }) 

    picker.on('change', pick => {
        if (pickerCallback) {
            pickerCallback(pick, picker);
        }
    }).update();

    this.show = function(callback) {
        pickerCallback = callback
        self.element.style.display = 'initial'
        picker.show();
    }

    this.hide = function() {
        picker.hide();
        self.element.style.display = 'none'
    }
    
    this.element.appendChild(colorPickerElement)
    document.body.appendChild(this.element)
}

module.exports.ColorPicker = ColorPicker