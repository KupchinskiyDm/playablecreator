const Label = function (parent, config) {
    const { Styles } = require("./Styles")

    this.parent = parent
    this.config = config
    this.element = document.createElement('studio-label')
    this.element.setAttribute('title', this.config.hint ? this.config.hint : this.config.text);
    this.element.className = this.config.style ? this.config.style : Styles.LABEL
    this.element.innerText = this.config.text
    
    this.parent.appendChild(this.element)
}

module.exports.Label = Label