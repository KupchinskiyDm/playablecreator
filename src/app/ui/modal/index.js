const CreateAnimWindow = function(){
    const { remote } = require('electron');
    const {EventManager} = require(path.resolve(__dirname, '../events/EventManager'));
    const Events = require(path.resolve(__dirname, '../events/events'));
    const self  = this
    this.open = function () {
        let win = new remote.BrowserWindow({
            width: 400,
            height: 200,
            resizable : false,
            minimizable : false,
            maximizable : false,
            parent: remote.getCurrentWindow(),
            modal: true,
            show: false
        })
        win.setMenu(null)
        win.on('closed', () => {
            win = null
            Creator.eventsManager.dispatchEvent(Creator.events.CLOSE_ANIM_CREATION_DLG, null, self)
        })
        win.loadURL(__dirname + '/index.html');
        win.once('ready-to-show', () => {
            win.show()
        })
    }
}    
exports.CreateAnimWindow = CreateAnimWindow