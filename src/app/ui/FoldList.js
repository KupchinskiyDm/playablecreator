const { ListItem } = require("./LisItem")

const FoldList = function (parent, config, callback) {
    const { Styles } = require("./Styles")
    const self = this
    this.parent = parent
    this.config = config
    this.mainElement = document.createElement('studio-label')
    this.mainElement.className = Styles.LIST_ITEM
    this.mainElement.innerText = ` ${this.config[0].name}  ↓`
   
    this.listBlock = document.createElement('list-block')
    this.listBlock.className = Styles.LIST_BLOCK_DISABLED
    this.listBlock.style.zIndex = 100000
    this.fold = function () {
        this.listBlock.className = Styles.LIST_BLOCK_DISABLED
    }

    this.mainElement.addEventListener('mouseover', (event) => {
        this.mainElement.className = Styles.LIST_ITEM_OVER
    });
    this.mainElement.addEventListener('mouseout', (event) => {
        this.mainElement.className = Styles.LIST_ITEM
    });
    this.mainElement.addEventListener('mousedown', (event) => {
        this.listBlock.className = this.listBlock.className === Styles.LIST_BLOCK ? Styles.LIST_BLOCK_DISABLED : Styles.LIST_BLOCK
    });

    this.config.forEach(element => {
        const item = new ListItem(self.listBlock, element, (source, connected) => { 
            connected.innerText = ` ${source.name}  ↓`
            self.fold()
            if (callback) {
                callback(source.value)
            }
        }, self.mainElement)
    })

    this.parent.appendChild(this.mainElement)
    this.parent.appendChild(this.listBlock)

    this.setValue = function (value, filter = element => element.value === value) {
        const confItem = this.config.find(filter);
        this.fold()
        if (confItem) {
            this.mainElement.innerText = ` ${confItem.name}  ↓`
        }
    }
}

module.exports.FoldList = FoldList