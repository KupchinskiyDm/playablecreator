const DragItem = (function() {
    let drag = null
    
    this.setItem = function(value){
        drag = value
    }

    this.getItem = function(){
        return drag
    }
    return this
})()

module.exports.DragItem = DragItem