const ColorField = function (parent, config, callback) {

    let current = '#000000'

    this.parent = parent
    this.config = config

    this.element = document.createElement('text')
    this.element.className = 'color-picker-field'
    
    this.parent.appendChild(this.element)

    this.setValue = function(value) {
        current = value
        this.element.style.background = value
    }

    this.getValue = function() {
        return current
    }
}

module.exports.ColorField = ColorField