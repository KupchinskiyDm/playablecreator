const Menu = function (){
    const self = this

    const container = document.createElement('div')
    container.id = "menu-container"
    document.body.appendChild(container)

    const addMenuItem = function (img, callback, hint = ''){
        const button =  document.createElement('div')
        button.className = 'menu-button'
        button.setAttribute('title', hint);
        const image = document.createElement('img')
        image.className = 'menu-button-image'
        image.src = img
        if (callback){
            button.addEventListener('mouseup', callback, false);
        }
        button.appendChild(image)
        container.appendChild(button)
    }
    
    addMenuItem('res/open.png', ()=>{
        Creator.eventsManager.dispatchEvent(Creator.events.OPEN_PROJECT_DLG, null, self)
    }, 'Open folder')
    addMenuItem('res/create.png', () => {
        Creator.eventsManager.dispatchEvent(Creator.events.CREATE_PARTICLES_EMITTER, null, self)
    }, 'Create particles emitter')
    // addMenuItem('res/save.png')
    // addMenuItem('res/publish.png')
    // addMenuItem('res/settings.png')
}
exports.Menu = Menu