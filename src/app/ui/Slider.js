const { Styles } = require("./Styles")

const Slider = function (parent, config, callback, connectedFiled) {
    this.parent = parent
    this.config = config
    this.element = document.createElement('input')
    this.element.className = this.config.style ? this.config.style : Styles.SLIDER
    this.element.type = 'range'
    this.element.min = this.config.min ? this.config.min : 0
    this.element.max = this.config.max ? this.config.max : 100
    this.element.value = this.config.value ? this.config.value : 100

    let lastValue = this.element.value

    this.element.oninput = () => {
        callback(this, connectedFiled, true, false)
    }

    this.element.onmousedown = () => {
        lastValue = this.element.value
    }

    this.element.onmouseup = () => {
        if (lastValue !== this.element.value) {
            lastValue = this.element.value
            callback(this, connectedFiled, false, true, lastValue);
        }
    }

    this.parent.appendChild(this.element)
}

module.exports.Slider = Slider