const TitleBar = function () {
    const ElectronTitlebarWindows = require('electron-titlebar-windows')
    const caption = document.createElement('bar-caption')
    const captiontext = document.createTextNode('')
    caption.appendChild(captiontext)

    const titlebar = new ElectronTitlebarWindows({
        backgroundColor: '#272e36',
        darkMode: true,
        draggable: true,
        fullscreen: true
    });
    
    titlebar.appendTo();

    this.setProjectCaption = function (value){
        caption.innerText = `Scene Creator`
    }


    this.setProjectCaption('-empty-')

    titlebar.titlebarElement.appendChild(caption);

    titlebar.on('close', () => {
        Creator.dialog.showMessageBox(Creator.electronApp.getCurrentWindow(),{
            message : 'Please confirm exit from app.', 
            type : 'question',
            buttons : ['Close', 'Cancel'],
            cancelId : 1
        }).then(result => {
            if (result.response === 0){
                Creator.electronApp.getCurrentWindow().close();
            }
        })
       
    });

    titlebar.on('fullscreen', () => {
        Creator.electronApp.getCurrentWindow().setFullScreen(true)
    });
    
    titlebar.on('minimize', () => {
        Creator.electronApp.getCurrentWindow().minimize();
    });
    
    titlebar.on('maximize', () => {
        Creator.electronApp.getCurrentWindow().setFullScreen(false);
        Creator.electronApp.getCurrentWindow().maximize();
    });

    Creator.electronApp.getCurrentWindow().maximize();
}

exports.TitleBar = TitleBar