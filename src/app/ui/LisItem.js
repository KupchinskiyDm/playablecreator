const { Styles } = require("./Styles")

const ListItem = function (parent, config, callback, connectedFiled) {
    this.parent = parent
    this.config = config
    this.value = this.config.value
    this.name = this.config.name
    this.element = document.createElement('studio-label')
    this.element.className = Styles.LIST_OPEN_ITEM
    this.element.innerText = this.config.name ? this.config.name : '-none-'
    
    this.element.addEventListener('mousedown', (event) => {
        callback(this, connectedFiled)
    })

    this.element.addEventListener('mouseover', (event) => {
        this.element.className = Styles.LIST_OPEN_ITEM_OVER
    });

    this.element.addEventListener('mouseout', (event) => {
        this.element.className = Styles.LIST_OPEN_ITEM
    });

    this.parent.appendChild(this.element);
}

module.exports.ListItem = ListItem