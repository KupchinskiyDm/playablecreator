module.exports.Styles = {
    BUTTON : 'properties-button',
    BUTTON_TEXTURED : 'properties-button-textured',
    BUTTON_IMAGE : 'properties-button-image',
    BUTTON_TEXT : 'properties-button-text',
    LABEL_CAPTION : 'block-container-caption',
    LABEL : 'block-container-label',
    INPUT_FIELD : 'properties-number-field',
    SLIDER : 'properties-slider',
    SLIDER_SHIFTED_TOP : 'properties-slider-shifted-top',
    LIST_OPEN_ITEM : 'list-open-item',
    LIST_OPEN_ITEM_OVER : 'list-open-item hover',
    LIST_ITEM : 'list-item',
    LIST_ITEM_OVER : 'list-item hover',
    LIST_BLOCK : 'list-block',
    LIST_BLOCK_DISABLED : 'list-block-disabled'
}