const { Styles } = require("./Styles")

const Button = function (parent, config, callback) {
    this.parent = parent
    this.element = document.createElement('div')
    this.element.className = config.text ? Styles.BUTTON : Styles.BUTTON_TEXTURED
    this.element.title = config.hint

    if (config.image){
        const image = document.createElement('img')
        image.className = Styles.BUTTON_IMAGE
        image.src = config.image
        this.element.appendChild(image)
    } else if (config.text) {
        const text = document.createElement('div')
        text.className = Styles.BUTTON_TEXT
        text.innerText = config.text
        this.element.appendChild(text)
    }

    this.element.addEventListener('mouseup', (event) => {
        callback && callback(this)
    })

    /*
    this.element.addEventListener('mouseover', (event) => {
        this.element.className = Styles.LIST_OPEN_ITEM_OVER
    });

    this.element.addEventListener('mouseout', (event) => {
        this.element.className = Styles.LIST_OPEN_ITEM
    });
    */

    this.parent.appendChild(this.element);
}

module.exports.Button = Button