const { Styles } = require("./Styles")

const InputField = function (parent, config, callback, connectedFiled) {
    this.parent = parent
    this.config = config
    this.element = document.createElement('input')
    this.element.className = this.config.style ? this.config.style : Styles.INPUT_FIELD
    this.element.type = 'text'
    this.element.value = this.config.value ? this.config.value : '1'

    this.element.addEventListener("focusout", () => callback(this, connectedFiled))
    this.element.addEventListener("focus",() => {
        this.element.select()
    })
    this.element.addEventListener('keydown', (event) => {
        if (event.keyCode === 13) {
            callback(this, connectedFiled)
        }
    })

    this.parent.appendChild(this.element);
}

module.exports.InputField = InputField