const AppModel = (function () {
    this.getInitialParticleEmitterData = function() {
        return {
            alpha : {
                list: [
                    {
                        value: 0.2,
                        time: 0
                    },
                    {
                        value: 1,
                        time: 0.33
                    },
                    {
                        value: 1,
                        time: 0.66
                    },
                    {
                        value: 0,
                        time: 1
                    }
                ],
            },
            scale: {
                list: [
                    {
                        value: 0.01,
                        time: 0
                    },
                    {
                        value: 0.5,
                        time: 0.33
                    },
                    {
                        value: 0.2,
                        time: 0.66
                    },
                    {
                        value: 0.01,
                        time: 1
                    }
                ],
            },
            color: {
                list: [
                    {
                        value: "#ffffff",
                        time: 0
                    },
                    {
                        value: "#ffffff",
                        time: 0.33
                    },
                    {
                        value: "#ffffff",
                        time: 0.66
                    },
                    {
                        value: "#ffffff",
                        time: 1
                    }
                ]
            },
            speed: {
                list: [
                    {
                        value: 40,
                        time: 0
                    },
                    {
                        value: 80,
                        time: 0.33
                    },
                    {
                        value: 400,
                        time: 0.66
                    },
                    {
                        value: 1000,
                        time: 1
                    }
                ],
            },
            pos: {
                x: 0,
                y: 0
            },
            acceleration: {
                x: 0,
                y: 0
            },
            startRotation: {
                min: 360,
                max: 0
            },
            rotationSpeed: {
                min: 0,
                max: 20
            },
            lifetime: {
                min: 1,
                max: 2
            },
            noRotation: false,
            addAtBack: false,
            blendMode: "add",
            maxSpeed: 0,
            frequency: 0.01,
            emitterLifetime: -1,
            maxParticles: 200,
            minimumScaleMultiplier: 10.5,
            minimumSpeedMultiplier: 0.5,
            spawnType: "point",
            //brust spawn type
            particlesPerWave: 1,
            particleSpacing: 0,
            angleStart: 0,
            //---
            spawnCircle: {
                x: 0,
                y: 0,
                r: 500,
                minR: 400
            },
            spawnRect: {
                x: -250,
                y: -250,
                w: 500,
                h: 500
            },
            entityType: "generated_particle_emitter-" + Creator.appVersion
        }
    }

    this.getParticleTimelineConfig = function (prop, frame, valueType = 'float') {
        return {
            captionX : 'T :',
            captionY : 'V :',
            hintX : 'Time 0-1',
            hintY : 'Value',
            propX : `emitterData.${prop}.list.${frame}.time`,
            propY : `emitterData.${prop}.list.${frame}.value`,
            valueType
        }
    }

    this.getParticleMinMaxConfig = function (prop, valueType = 'float') {
        return {
            captionX : 'Min :',
            captionY : 'Max :',
            hintX : 'Minimum',
            hintY : 'Maximum',
            propX : `emitterData.${prop}.min`,
            propY : `emitterData.${prop}.max`,
            valueType
        }
    }

    this.getParticleXYConfig = function (prop, valueType = 'float') {
        return {
            captionX : 'X :',
            captionY : 'Y :',
            hintX : 'X',
            hintY : 'Y',
            propX : `emitterData.${prop}.x`,
            propY : `emitterData.${prop}.y`,
            valueType
        }
    }

    this.getParticlePropConfig = function (prop, caption = '', valueType = 'float') {
        return {
            caption : caption ? caption : prop.capitalize().splitCamelCase() + ' :',
            hint : caption ? caption : prop.capitalize().splitCamelCase(),
            prop : `emitterData.${prop}`,
            valueType
        }
    }

    return this;
})();

module.exports.AppModel = AppModel;